<?php

namespace App\Http\Controllers;

use App\Model\ProductAttribute;
use App\Model\ProductAttributeList;
use App\Model\ProductCategory;
use App\Model\ProductVariation;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

use Illuminate\Http\Request;

class ProductVariationController extends Controller
{

    public function store(Request  $request){

        $collection= new ProductVariation();
        $collection->size=$request->size;
        $collection->color= $request->color;
        $collection->price=$request->price;
        $collection->product_id=$request->product_id;
        $collection->quantity=$request->quantity;
        $collection->weight=$request->weight;
        $collection->length=$request->length;
        $collection->wide=$request->wide;
        $collection->height=$request->height;
        $collection->sku=$request->sku;
        $collection->image=$request->images[0];
        $collection->save();
        return Redirect::back()->with('msg', 'The Message');
    }
    public function edit($id){
        $parent=ProductCategory::where('parent_id',0)->get();
        $category=ProductCategory::findOrFail($id);

        return view('products.categories.edit',[
            'parent'=>$parent,
            'category'=>$category
        ]);
    }
    public function editPost(Request $request,$id){
        $collection=  ProductCategory::find($id);
        $collection->name=$request->name;
        $collection->slug= Str::slug($request->name, '-');
        $collection->parent_id=$request->parent_id;
        $collection->description=$request->description;
        $collection->status=$request->status;
        $collection->image=$request->image;
        $collection->save();
        return view('products.categories.index');
    }
    public function destroy(Request $request){
        ProductCategory::findOrFail($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'

        ],'200');
    }


    public function getVaration($id){
        $data=ProductVariation::findOrFail($id);
        $attributes=ProductAttribute::all();
        foreach ($attributes as $item){
            $attributes_list=ProductAttributeList::where('parent_id',$item->id)->get()->toArray();
            $item->lists=$attributes_list;
        }
        $views=get_variation($data,$attributes);
        return response()->json([
            'data'=>$views
        ],'200');
    }

    public function postVaration(Request $request,$id){
        $data=ProductVariation::findOrFail($id);
        $data->size=$request->attribute_Size;
        $data->color=$request->attribute_Color;
        $data->sku=$request->sku;
        $data->price=$request->price;
        $data->quantity=$request->quantity;
        $data->weight=$request->weight;
        $data->length=$request->length ;
        $data->wide=$request->wide ;
        $data->height=$request->height ;
        $data->image=$request->images[0]?$request->images[0]:null;
        $data->save();
        return Redirect::back()->with('msg', 'The Message');

    }
}
