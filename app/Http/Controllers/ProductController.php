<?php

namespace App\Http\Controllers;


use App\Model\Product;
use App\Model\ProductAttributeList;
use App\Model\ProductCategory;
use App\Model\ProductCollection;
use App\Model\ProductImage;
use App\Model\ProductVariation;
use Assets;

use Botble\Table\TableBuilder;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\ProductAttribute;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{

    /**
     * @return Factory|View
     *
     * @throws Throwable
     */
    public function getIndex(Request $request, TableBuilder $tableBuilder)
    {
        return view('products.index');
    }
    public function getProductData()
    {

        $data=Product::all();
        return  DataTables::of($data)
            ->addColumn('image', function ($data) {
                $url= asset('storage/'.$data['image']);
                return '<img src="'.$url.'" width="50" height=30 alt="'.$data['name'].'">';
            })
            ->addColumn('action', function ($data) {
                return '<a href="'. route('product.edit', $data->id) .'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a> <a href="javascript:void(0)" data-id="' . $data->id . '" class="btn deleteItem btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
            })
            ->rawColumns([ 'action','image'])
            ->make(true);

    }
    public function getProductAttributeList(Request $request){
        $item_attribute=ProductAttributeList::where('parent_id',$request->id)->get()->toArray();
        return response()->json(['data'=>$item_attribute]);
    }
    public function createProduct(){
        $product_attributes=ProductAttribute::all();
        $collection=ProductCollection::all();
        $categories=ProductCategory::all();
        return view('products.add',[
            'product_attributes'=>$product_attributes,
            'collection'=>$collection,
            'categories'=>$categories
        ]);
    }

    public function createProductPost(Request $request){
        $product=new Product();
        $product->name=$request->name;
        $product->slug= Str::slug($request->name, '-');
        $product->sku=$request->sku;
        $product->price=$request->price;

        $product->content=$request->contents;
        $product->image=$request->images[0];
        $product->description=$request->description;
        $product->categories=json_encode($request->product_categories);
        $product->collections=json_encode($request->product_collections);



        $product->save();
        $product_var=new ProductVariation();
        $product_var->product_id=$product->id;
        $product_var->price=$request->price;
        $product_var->weight=$request->weight;
        $product_var->image=$request->images[0];
        $product_var->length=$request->length;
        $product_var->wide=$request->wide;
        $product_var->height=$request->height;
        $product_var->save();

        if(count($request->images) >1){
            foreach ($request->images as $key =>  $item){
                if($key){
                    $product_images=new ProductImage();
                    $product_images->product_id = $product->id;
                    $product_images->name = $item;
                    $product_images->save();
                }
            }
        }
        return redirect()->back();
    }

    public function editProduct($id){
        $attributes=ProductAttribute::all();
        foreach ($attributes as $item){
            $attributes_list=ProductAttributeList::where('parent_id',$item->id)->get()->toArray();
            $item->lists=$attributes_list;
        }

        $data=Product::findOrFail($id);
        $product_var=ProductVariation::where('product_id',$id)->get();

        $product_attributes=ProductAttribute::all();
        $collection=ProductCollection::all();
        $categories=ProductCategory::all();
        return view('products.edit',[
            'product_attributes'=>$product_attributes,
            'collection'=>$collection,
            'categories'=>$categories,
            'data'=>$data,
            'product_var'=>$product_var,
            'attributes'=>$attributes
        ]);
    }

    public function editProductPost(Request $request,$id){
        $data=Product::findOrFail($id);
        $data->name=$request->name;
        $data->description=$request->description;
        $data->content=$request->contents;
        $data->sku=$request->sku;
        $data->price=$request->price;
        $data->categories=json_encode($request->product_categories);
        $data->collections=json_encode($request->product_collections);
        $data->save();
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProduct(Request $request)
    {
        Product::findOrFail($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'

        ],'200');
    }








    public function getProductAtt(){
       return view('products.attributes.index');
    }
    public function getProductAttData()
    {
         $data=ProductAttribute::all();
        return  DataTables::of($data)
            ->addColumn('action', function ($data) {
                return '<a href="'. route('product.attribute.edit', $data->id) .'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a> <a href="javascript:void(0)" data-id="' . $data->id . '" class="btn deleteItem btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
            })
            ->rawColumns([ 'action'])
            ->make(true);

    }

    public function createProductAtt(){
        return view('products.attributes.add');
    }

    public function createProductAttPost(Request $request){

        $data= new ProductAttribute;
        $data->name=$request->title;
        $data->slug=$request->slug;
        $data->status=$request->status;
        $data->order=$request->order;
        $data->save();
        if($request->attribute_title) {
            foreach ($request->attribute_title as $key => $value) {
                $data_att = new ProductAttributeList;
                $data_att->parent_id = $data->id;
                $data_att->name = $value;
                $data_att->slug = $request->attribute_slug[$key];
                $data_att->save();
            }
        }
        return view('products.attributes.index');
    }

    public function editProductAtt( $id){
        $data=\App\Model\ProductAttribute::findOrFail($id);
        $data_attributes=ProductAttributeList::where('parent_id',$id)->get()->toArray();

        return view('products.attributes.edit',[
            'data'=>$data,
            'data_attributes'=>$data_attributes
        ]);

    }

    public function editProductAttPost(Request $request)
    {

        $data=  ProductAttribute::find($request->id);
        $data->name=$request->title;
        $data->slug=$request->slug;
        $data->status=$request->status;
        $data->order=$request->order;
        $data->save();
        if($request->attribute_title){
            foreach ($request->attribute_title as $key => $value){
                $data_att=new ProductAttributeList;
                $data_att->parent_id=$data->id;
                $data_att->name=$value;
                $data_att->slug=$request->attribute_slug[$key];
                $data_att->save();
            }
        }

        if($request->id_attribute_item){
            foreach ($request->id_attribute_item as $key => $value){
                $data_att_edit=ProductAttributeList::find($value);
                $data_att_edit->name=$request->attribute_title_old[$key];
                $data_att_edit->slug=$request->attribute_slug_old[$key];
                $data_att_edit->save();
            }
        }
        return view('products.attributes.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProductAttribute(Request $request)
    {
        ProductAttribute::find($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'

        ],'200');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProductAttributeList(Request $request)
    {
        ProductAttributeList::find($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'
        ],'200');
    }
}
