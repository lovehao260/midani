<?php

namespace App\Http\Controllers;

use App\Model\ProductCollection;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Collection;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class ProductCollectionController extends Controller
{
    public function getIndex(){
            return view('products.collections.index');
    }

    public function indexData(){
        $data=ProductCollection::all();

        return  DataTables::of($data)

            ->addColumn('image', function ($data) {
                $url= asset('storage/'.$data['image']);
                return '<img src="'.$url.'" width="50" height=30 alt="'.$data['name'].'">';
            })
            ->addColumn('action', function ($data) {
                return '<a href="'. route('product.collection.edit', $data->id) .'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a> <a href="javascript:void(0)" data-id="' . $data->id . '" class="btn deleteItem btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
            })
            ->rawColumns([ 'action','image'])
            ->make(true);
    }

    public function create(){
        return view('products.collections.add');
    }
    public function store(Request  $request){
            $collection= new ProductCollection();
            $collection->name=$request->name;
             $collection->slug= Str::slug($request->slug, '-');
            $collection->description=$request->description;
            $collection->status=$request->status;
            $collection->image=$request->image;
            $collection->save();
        return view('products.collections.index');
    }
    public function edit($id){

        $collection=ProductCollection::findOrFail($id);

        return view('products.collections.edit',[
            'collection'=>$collection
        ]);
    }
    public function editPost(Request $request,$id){

        $collection=  ProductCollection::find($id);
        $collection->name=$request->name;
        $collection->slug= Str::slug($request->slug, '-');
        $collection->description=$request->description;
        $collection->status=$request->status;
        $collection->image=$request->image;
        $collection->save();
        return view('products.collections.index');
    }
    public function destroy(Request $request){
        ProductCollection::findOrFail($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'

        ],'200');
    }
}
