<?php

namespace App\Http\Controllers;

use App\Model\ProductCategory;
use App\Model\ProductCollection;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Collection;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class ProjectCategoryController extends Controller
{
    public function getIndex(){
        return view('products.categories.index');
    }

    public function indexData(){
        $data=ProductCategory::all();

        return  DataTables::of($data)
            ->addColumn('image', function ($data) {
                $url= asset('storage/'.$data['image']);
                return '<img src="'.$url.'" width="50" height=30 alt="'.$data['name'].'">';
            })
            ->addColumn('action', function ($data) {
                return '<a href="'. route('product.categories.edit', $data->id) .'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> View</a> <a href="javascript:void(0)" data-id="' . $data->id . '" class="btn deleteItem btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
            })
            ->rawColumns([ 'action','image'])
            ->make(true);
    }

    public function create(){
        $parent=ProductCategory::where('parent_id',0)->get();
        return view('products.categories.add',['parent'=>$parent]);
    }
    public function store(Request  $request){
        $collection= new ProductCategory();
        $collection->name=$request->name;
        $collection->slug= Str::slug($request->name, '-');
        $collection->parent_id=$request->parent_id;
        $collection->description=$request->description;
        $collection->status=$request->status;
        $collection->image=$request->image;
        $collection->save();
        return view('products.categories.index');
    }
    public function edit($id){
        $parent=ProductCategory::where('parent_id',0)->get();
        $category=ProductCategory::findOrFail($id);

        return view('products.categories.edit',[
            'parent'=>$parent,
            'category'=>$category
        ]);
    }
    public function editPost(Request $request,$id){
        $collection=  ProductCategory::find($id);
        $collection->name=$request->name;
        $collection->slug= Str::slug($request->name, '-');
        $collection->parent_id=$request->parent_id;
        $collection->description=$request->description;
        $collection->status=$request->status;
        $collection->image=$request->image;
        $collection->save();
        return view('products.categories.index');
    }
    public function destroy(Request $request){
        ProductCategory::findOrFail($request->id)->delete();
        return response()->json([
            'messages'=>'Item deleted successfully.'

        ],'200');
    }
}
