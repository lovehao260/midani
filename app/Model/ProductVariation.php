<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $table = 'product_variations';
    protected $fillable = [
        'id','product_id', 'image', 'price', 'length', 'wide', 'height', 'weight', 'image','sku'
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];

}
