<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeList extends Model
{
    protected $table = 'product_attribute_lists';
    protected $fillable = [
        'name', 'slug','parent_id',
    ];
    public function Attribute(){
        return $this->belongsTo(ProductAttribute::class,'attribute_id');
    }
}
