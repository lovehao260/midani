<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';
    protected $fillable = [
        'id','name', 'slug',
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];

    public function attributeLists(){
        return $this->hasMany(ProductAttributeList::class, 'parent_id','id');
    }
}
