<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCollection extends Model
{
    protected $table = 'product_collections';
    protected $fillable = [
        'id','name','slug','description','image','status'
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];
}
