<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    protected $fillable = [
        'id','name', 'parent_id','slug','description','image','status'
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];
}
