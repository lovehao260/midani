<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductCollectionImage extends Model
{
    protected $table = 'product_collection_images';
    protected $fillable = [
        'id','collection_id','image'
    ];

}
