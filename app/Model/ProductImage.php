<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $fillable = [
        'id','name', 'product_id',
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];

}
