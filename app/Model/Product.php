<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'id','name', 'slug','sku','price','description','content','order','status','image','categories','collections'
    ];
    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'json',
    ];
}
