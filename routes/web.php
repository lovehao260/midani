<?php
Route::group(['prefix' => 'admin/ecommerce/'], function () {

    //Product Attribute
    Route::group(['prefix' => '/product-attribute-sets'], function () {
        Route::get('/', [
            'as'         => 'product.attribute.index',
            'uses'       => 'ProductController@getProductAtt',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/data', [
            'as'         => 'product.attribute.data',
            'uses'       => 'ProductController@getProductAttData',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/create', [
            'as'         => 'product.attribute.create',
            'uses'       => 'ProductController@createProductAtt',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/create', [
            'as'         => 'product.attribute.create.post',
            'uses'       => 'ProductController@createProductAttPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/edit/{id}', [
            'as'         => 'product.attribute.edit',
            'uses'       => 'ProductController@editProductAtt',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/edit/{id}', [
            'as'         => 'product.attribute.edit.post',
            'uses'       => 'ProductController@editProductAttPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::DELETE('/destroy', [
            'as'         => 'product.attribute.destroy.post',
            'uses'       => 'ProductController@destroyProductAttribute',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::DELETE('/destroy_list', [
            'as'         => 'product.attribute.destroy_list.post',
            'uses'       => 'ProductController@destroyProductAttributeList',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
    });

    //Product
    Route::group(['prefix' => '/products'], function () {
        Route::get('', [
            'as'         => 'product.index',
            'uses'       => 'ProductController@getIndex',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/data/products', [
            'as'         => 'product.data',
            'uses'       => 'ProductController@getProductData',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/create', [
            'as'         => 'product.create',
            'uses'       => 'ProductController@createProduct',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/create', [
            'as'         => 'product.create.post',
            'uses'       => 'ProductController@createProductPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/product-attribute-list', [
            'as'         => 'product.attribute.list.index',
            'uses'       => 'ProductController@getProductAttributeList',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::DELETE('/destroy', [
            'as'         => 'product.destroy.post',
            'uses'       => 'ProductController@destroyProduct',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::get('/edit/{id}', [
            'as'         => 'product.edit',
            'uses'       => 'ProductController@editProduct',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/edit/{id}', [
            'as'         => 'product.edit.post',
            'uses'       => 'ProductController@editProductPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

    });

    //Collection
    Route::group(['prefix' => '/product-collections'], function () {
        Route::get('/', [
            'as'         => 'product.collection.index',
            'uses'       => 'ProductCollectionController@getIndex',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/data', [
            'as'         => 'product.collection.data',
            'uses'       => 'ProductCollectionController@indexData',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::get('/create', [
            'as'         => 'product.collection.create',
            'uses'       => 'ProductCollectionController@create',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/create', [
            'as'         => 'product.collection.create.post',
            'uses'       => 'ProductCollectionController@store',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::DELETE('/destroy', [
            'as'         => 'product.collection.destroy.post',
            'uses'       => 'ProductCollectionController@destroy',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::get('/edit/{id}', [
            'as'         => 'product.collection.edit',
            'uses'       => 'ProductCollectionController@edit',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/edit/{id}', [
            'as'         => 'product.collection.edit.post',
            'uses'       => 'ProductCollectionController@editPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
    });

    //Category
    Route::group(['prefix' => '/product-categories'], function () {
        Route::get('/', [
            'as'         => 'product.categories.index',
            'uses'       => 'ProjectCategoryController@getIndex',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/data', [
            'as'         => 'product.categories.data',
            'uses'       => 'ProjectCategoryController@indexData',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::get('/create', [
            'as'         => 'product.categories.create',
            'uses'       => 'ProjectCategoryController@create',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/create', [
            'as'         => 'product.categories.create.post',
            'uses'       => 'ProjectCategoryController@store',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::DELETE('/destroy', [
            'as'         => 'product.categories.destroy.post',
            'uses'       => 'ProjectCategoryController@destroy',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::get('/edit/{id}', [
            'as'         => 'product.categories.edit',
            'uses'       => 'ProjectCategoryController@edit',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/edit/{id}', [
            'as'         => 'product.categories.edit.post',
            'uses'       => 'ProjectCategoryController@editPost',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
    });

    Route::group(['prefix' => '/product-variations'], function () {
        Route::post('/store', [
            'as'         => 'product.variations.post',
            'uses'       => 'ProductVariationController@store',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);

        Route::get('/get-product-variation/{id}', [
            'as'         => 'product.variations.get',
            'uses'       => 'ProductVariationController@getVaration',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
        Route::post('/edit-product-variation/{id}', [
            'as'         => 'product.variations.edit.post',
            'uses'       => 'ProductVariationController@postVaration',
            'permission' => ACL_ROLE_SUPER_USER,
        ]);
    });
});
