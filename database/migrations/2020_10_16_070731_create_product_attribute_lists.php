<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_lists', function (Blueprint $table) {

            $table->increments('id');
            $table->bigInteger('parent_id')->unsigned()->index();

            $table->string('name');
            $table->timestamps();
            $table->foreign('parent_id')->references('id')
                ->on('product_attributes')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_lists');
    }
}
