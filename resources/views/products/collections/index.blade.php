@extends('core/base::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-table">
                    <div class="card-header">
                        <a href="{{route('product.collection.create')}}" class="btn btn-primary btn-sm float-right"><i
                                class="fa fa-plus"></i> Create</a>
                    </div>
                    <div class="card-body ">
                        <table id="list-product-index" class="table table-bordered  table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Hình Ảnh</th>
                                <th>Tên</th>
                                <th>Slug</th>
                                <th>Ngày Tạo</th>
                                <th>Trạng thái</th>
                                <th style="width: 130px">Tác Vụ</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- confirm dialog-->
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <strong>{{ trans('core/table::general.confirm_delete') }}</strong>
                </div>
                <div id="confirmMessage" class="modal-body">
                    {{ trans('core/table::general.confirm_delete_msg') }}
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmCancel" class="btn btn-default btn-cancel" data-dismiss="modal">
                        {{ trans('core/table::general.cancel') }}
                    </button>
                    <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteItem($(this).val())">
                        {{ trans('core/table::general.delete') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

@stop

@push('scripts')
    <script>

        let table = $('#list-product-index').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('product.collection.data') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'image', name: 'image' },
                { data: 'name', name: 'name' },
                { data: 'slug', name: 'slug' },
                { data: 'created_at', name: 'created_at' },
                { data: 'status', name: 'status' },
                { data: 'action', name: 'action' }
            ]
        });

        $('#list-product-index').on('click', '.deleteItem', function () {
            let Item_id = $(this).data("id");
            $('#confirmDeleteItem').modal('toggle');
            $('#btnConfirmDelete').val(Item_id);

        });
        function deleteItem(id){
            request = $.ajax({
                url: "{{ route('product.collection.destroy.post') }}",
                method: "Delete",
                data: {
                    "_token" : "{{ csrf_token() }}",
                    "id" : id
                }
            }).done(function (data) {
                $("#confirmDeleteItem").modal('toggle');
                table.draw();
            })
        }



    </script>
@endpush
