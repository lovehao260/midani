@extends('core/base::layouts.master')
@section('content')
    <form method="POST" action="{{route('product.collection.edit.post',$collection->id)}}" accept-charset="UTF-8">
        @csrf
        <div class="row">
            <div class="col-md-9">
                <div class="main-form">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="name" class="control-label required" aria-required="true">Name</label>
                            <input class="form-control" placeholder="Name" data-counter="120" name="name" type="text"
                                   id="name" value="{{$collection->name}}">
                            <p class="help-block">
                                Label key: <code></code>. We will use this key for filter. </p>
                        </div>
                        <div class="form-group">
                            <label for="slug" class="control-label required">Slug</label>
                            <input class="form-control" data-counter="120" name="slug" type="text" id="slug"
                                   value="{{$collection->slug}}">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Description</label>
                            <textarea class="form-control" rows="4" placeholder="Description" data-counter="400"
                                      name="description" cols="50" id="description">
                                {!!$collection->description!!}
                            </textarea>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 right-sidebar">
                <div class="widget meta-boxes form-actions form-actions-default action-horizontal">
                    <div class="widget-title">
                        <h4>
                            <span>Publish</span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="btn-set">
                            <button type="submit" name="submit" value="save" class="btn btn-info">
                                <i class="fa fa-save"></i> Save
                            </button>
                            &nbsp;
                            <button type="submit" name="submit" value="apply" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> Save &amp; Edit
                            </button>
                        </div>
                    </div>
                </div>
                <div id="waypoint"></div>

                <div class="widget meta-boxes">
                    <div class="widget-title">
                        <h4><label for="status" class="control-label required">Status</label></h4>
                    </div>
                    <div class="widget-body">
                        <div class="ui-select-wrapper">
                            <select class="form-control ui-select ui-select" id="status" name="status">
                                <option value="0">Published</option>
                                <option value="1">Draft</option>
                                <option value="2">Pending</option>
                            </select>
                            <svg class="svg-next-icon svg-next-icon-size-16">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="widget meta-boxes">
                    <div class="widget-title">
                        <h4><label for="image" class="control-label">Image</label></h4>
                    </div>
                    <div class="widget-body">
                        <div class="image-box">
                            <input type="hidden" name="image" value="{{$collection['image']}}" class="image-data">
                            <div class="preview-image-wrapper ">
                                <img src="{{asset('storage/'.$collection['image'])}}" alt="preview image"
                                     class="preview_image" width="150">
                                <a class="btn_remove_image" title="Remove image">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_gallery" data-result="image" data-action="select-image">
                                    Choose image
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop

@push('scripts')
    <script>
    </script>
@endpush
