<div id="main-manage-product-type">
    <div class="widget meta-boxes">
        <div class="widget-title">
            <h4>
                <span> Product has variations</span>
            </h4>
        </div>
        <div class="widget-body">
            <div id="product-variations-wrapper">
                <div class="variation-actions">
                    <a href="#" class="btn-trigger-add-new-product-variation">Add variation</a>
                </div>
                <table class="table table-hover-variants">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Size</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product_var as $item)
                        <tr>
                            <td>
                                <div class="wrap-img-product">
                                    <img src="{{ get_object_image($item['image'], Arr::get($item['image'], 'allow_thumb', true) == true ? 'thumb' : null) }}"
                                         alt="{{ __('preview image') }}" class="preview_image" @if (Arr::get($item['image'], 'allow_thumb', true)) width="150"
                                        @endif>
                                </div>
                            </td>
                            <td>{{$item->size}}</td>
                            <td>{{$item->color}}</td>
                            <td>{{$item->price}}</td>
                            <td style="width: 180px;" class="text-center">
                                <a href="#" class="btn btn-info btn-trigger-edit-product-version"
                                   data-load-form="{{route('product.variations.get',$item->id)}}"
                                   data-load-edit-post="{{route('product.variations.edit.post',$item->id)}}">Edit</a>
                                <a href="#" data-target="https://hasa.botble.com/admin/ecommerce/products/delete-version/25"
                                   class="btn-trigger-delete-version btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="add-new-product-variation-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <form method="POST" action="{{route('product.variations.post')}}" accept-charset="UTF-8">
        @csrf
    <div class="modal-dialog modal-lg  ">
        <input hidden type="text" value="{{$data->id}}" name="product_id">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h4 class="modal-title"><i class="til_img"></i><strong>Add new variation</strong></h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body with-padding">
                <div class="variation-form-wrapper">
                    <div class="row">
                        @foreach($attributes as $item)
                            @if($item->lists)
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="attribute-size1"
                                               class="text-title-field required">{{$item->name}}</label>
                                        <div class="ui-select-wrapper">
                                            <select class="ui-select"
                                                    id="attribute-size"
                                                    name="attribute-{{$item->name}}"
                                                    aria-invalid="false">
                                                @foreach($item->lists as $list)
                                                    <option value="{{$list['id']}}">
                                                        {{$list['name']}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <svg
                                                class="svg-next-icon svg-next-icon-size-16">
                                                <use
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    xlink:href="#select-chevron"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>
                    <div class="row price-group">
                        <input type="hidden" value="0"
                               class="detect-schedule hidden" name="sale_type">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label class="text-title-field">Sku</label>
                                <input class="next-input"
                                       data-counter="30" name="sku" type="text"><small
                                    class="charcounter">(30 character(s)
                                    remain)</small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="text-title-field">Price</label>
                                <div class="next-input--stylized"><span class="next-input-add-on next-input__add-on--before">$</span>
                                    <input name="price" class="next-input input-mask-number regular-price next-input--invisible"
                                           step="any" value="0" type="number">
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>

                    <div class="storehouse-info ">
                        <div class="form-group">
                            <label class="text-title-field">Quantity</label>
                            <input type="text"
                                   class="next-input input-mask-number input-medium"
                                   value="13" name="quantity">
                        </div>
                        <div class="form-group">
                            <label class="text-title-field">
                                <input type="hidden"
                                       name="allow_checkout_when_out_of_stock"
                                       value="0">
                                <input type="checkbox"
                                       name="allow_checkout_when_out_of_stock"
                                       class="hrv-checkbox" value="1">
                                &nbsp;Allow customer checkout when this product
                                out of stock
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="shipping-management">
                        <label class="text-title-field">Shipping</label>
                        <div class="row">
                            <div class="col-md-3 col-md-6">
                                <div class="form-group">
                                    <label>Weight (g)</label>
                                    <div class="next-input--stylized">
                                         <span class="next-input-add-on next-input__add-on--before">g</span>
                                        <input type="text"
                                               class="next-input input-mask-number next-input--invisible"
                                               name="weight" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="form-group">
                                    <label>Length (cm)</label>
                                    <div class="next-input--stylized">
                                          <span class="next-input-add-on next-input__add-on--before">cm</span>
                                        <input type="text"
                                               class="next-input input-mask-number next-input--invisible"
                                               name="length" value="0"
                                               im-insert="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="form-group">
                                    <label>Wide (cm)</label>
                                    <div class="next-input--stylized">
                                       <span class="next-input-add-on next-input__add-on--before">cm</span>
                                        <input type="text"
                                               class="next-input input-mask-number next-input--invisible"
                                               name="wide" value="0"
                                               im-insert="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="form-group">
                                    <label>Height (cm)</label>
                                    <div class="next-input--stylized">
                                       <span class="next-input-add-on next-input__add-on--before">cm</span>
                                        <input type="text"
                                               class="next-input input-mask-number next-input--invisible"
                                               name="height" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="variation-images"
                         style="position: relative; border: 1px dashed #ccc; padding: 10px;">
                        <div class="product-images-wrapper">
                            <a href="#"
                               class="add_image_list"
                               data-name="images[]">Add image
                            </a>
                            <div class="images-wrapper">
                                <div data-name="images[]"
                                     class="text-center cursor-pointer js-btn-trigger-add-image default-placeholder-product-image ">
                                    <img src="https://hasa.botble.com/vendor/core/core/base/images/placeholder.png"
                                        alt="Image" width="120">
                                    <br>
                                    <p style="color:#c3cfd8">Using button
                                        <strong>Select image</strong> to add
                                        more images.</p>
                                </div>
                                <ul class="list-unstyled list-gallery-media-images clearfix hidden ui-sortable"
                                    style="padding-top: 20px;">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="float-left btn btn-warning" data-dismiss="modal">
                    Cancel
                </button>
                <button class="float-right btn btn-info" type="submit">
                    Save changes
                </button>

            </div>
        </div>
    </div>
    </form>
</div>

<div id="edit-product-variation-modal" class="modal fade" tabindex="-1"
     data-backdrop="static" data-keyboard="false" style="display: none;"
     aria-hidden="true">
    <div class="modal-dialog    modal-lg  ">
        <div class="modal-content">
            <form method="POST" action="" accept-charset="UTF-8" id="edit_variation_product">
                @csrf
            <div class="modal-header bg-info">
                <h4 class="modal-title"><i class="til_img"></i><strong>Edit
                        variation</strong></h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body with-padding">

            </div>
            <div class="modal-footer">
                <button class="float-left btn btn-warning" data-dismiss="modal">
                    Cancel
                </button>
                <button class="float-right btn btn-info" type="submit">Save
                    changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

