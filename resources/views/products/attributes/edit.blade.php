@extends('core/base::layouts.master')
@section('content')
    <div id="main">
        <form method="POST" action="{{route('product.attribute.edit.post',$data->id)}}"
              accept-charset="UTF-8" class="update-attribute-set-form">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    <div class="main-form">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="title" class="control-label required" >Tiêu đề</label>
                                <input class="form-control" data-counter="120" name="title" type="text" required
                                       id="title" value="{{$data->name}}"><small class="charcounter">(120 kí tự còn lại)</small>
                            </div>
                            <div class="form-group">
                                <label for="slug" class="control-label required" >Slug</label>
                                <input class="form-control" data-counter="120" name="slug" required type="text" id="slug" value="{{$data->slug}}">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4>
                                <span> Attributes list</span>
                            </h4>
                        </div>
                        <div class="widget-body">
                            <script id="product_attribute_template" type="text/x-custom-template">
                                <li  class="clearfix">
                                    <div class="swatch-is-default">
                                        <input type="radio" name="related_attribute_is_default" value="">
                                    </div>
                                    <div class="swatch-title">
                                        <input type="text" class="form-control" value="" name="attribute_title[]" required>
                                    </div>
                                    <div class="swatch-slug">
                                        <input type="text" class="form-control" value="" name="attribute_slug[]" required>
                                    </div>

                                    <div class="remove-item"><a href="#" class="font-red"><i
                                                class="fa fa-trash"></i></a></div>
                                </li>
                            </script>
                            <div class="swatches-container">
                                <div class="header clearfix">
                                    <div class="swatch-is-default">
                                        Is default
                                    </div>
                                    <div class="swatch-title">
                                        Title
                                    </div>
                                    <div class="swatch-slug">
                                        Slug
                                    </div>
                                    <div class="remove-item">Xóa</div>
                                </div>
                                <ul class="swatches-list ui-sortable">
                                    @foreach($data_attributes as $item)
                                        <li class="clearfix">
                                            <input type="text" name="id_attribute_item[]" value="{{$item['id']}}" hidden>
                                            <div class="swatch-is-default">
                                                <input type="radio" name="related_attribute_is_default">
                                            </div>
                                            <div class="swatch-title">
                                                <input type="text" class="form-control"  value="{{$item['name']}}" name="attribute_title_old[]" required>
                                            </div>
                                            <div class="swatch-slug">
                                                <input type="text" class="form-control"  value="{{$item['slug']}}" name="attribute_slug_old[]" required>
                                            </div>
                                            <div class=" remove-item-old" onclick="removeAttributeItem({{$item['id']}})" ><a href="#" class="font-red"><i
                                                        class="fa fa-trash"></i></a></div>
                                        </li>
                                    @endforeach
                                </ul>
                                <button type="button" class="btn purple js-add-new-attribute">Add new attribute</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 right-sidebar">
                    <div class="widget meta-boxes form-actions form-actions-default action-horizontal">
                        <div class="widget-title">
                            <h4>
                                <span>Xuất bản</span>
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="btn-set">
                                <button type="submit" name="submit" value="save" class="btn btn-info">
                                    <i class="fa fa-save"></i> Lưu
                                </button>
                                &nbsp;
                                <button type="submit" name="submit" value="apply" class="btn btn-success">
                                    <i class="fa fa-check-circle"></i> Lưu &amp; chỉnh sửa
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="waypoint"></div>
                    <div class="form-actions form-actions-fixed-top hidden">

                        <div class="btn-set">
                            <button type="submit" name="submit" value="save" class="btn btn-info">
                                <i class="fa fa-save"></i> Lưu
                            </button>
                            &nbsp;
                            <button type="submit" name="submit" value="apply" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> Lưu &amp; chỉnh sửa
                            </button>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="status" class="control-label required">Trạng thái</label></h4>
                        </div>
                        <div class="widget-body">
                            <div class="ui-select-wrapper">
                                <select class="form-control ui-select ui-select" id="status" name="status">
                                    <option value="0" {{ ( $data['status'] == 0) ? 'selected' : '' }}>Published</option>
                                    <option value="1" {{ ( $data['status'] == 1) ? 'selected' : '' }}>Bản nháp</option>
                                    <option value="2" {{ ( $data['status'] == 2) ? 'selected' : '' }}>Đang chờ xử lý</option>
                                </select>
                                <svg class="svg-next-icon svg-next-icon-size-16">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="order" class="control-label">Thứ tự</label></h4>
                        </div>
                        <div class="widget-body">
                            <input class="form-control" placeholder="Sắp xếp" name="order" type="number" value="{{$data['order']}}"
                                   id="order">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $('.js-add-new-attribute').click(function () {

            let html =$("#product_attribute_template").html();
            $('.swatches-list').append(html);
        })
        $('.list_attribute ,.swatches-list').on('click', '.remove-item', function (e) {
            e.preventDefault();
            $(this).parent().remove();
        });
        $(".product-select-attribute-item").change(function () {
            $.ajax({
                url: "{{ route('product.attribute.list.index') }}",
                method: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).val()
                }
            }).done(function (data) {

                data.data.forEach(function (item, index) {
                    $('.product-select-attribute-item-value').append(`<option value="${item.id}">${item.name}</option>`)
                });
            })
        }).change();
        function removeAttributeItem(id){
            request = $.ajax({
                url: "{{ route('product.attribute.destroy_list.post') }}",
                method: "Delete",
                data: {
                    "_token" : "{{ csrf_token() }}",
                    "id" : id
                }
            })
        }
        $('.list_attribute ,.swatches-list').on('click', '.remove-item-old', function (e) {
            e.preventDefault();
            $(this).parent().remove();

        });
    </script>
@stop
