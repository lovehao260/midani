@extends('core/base::layouts.master')
@section('content')
    <div id="main">
        <form method="POST" action="{{route('product.edit.post',$data->id)}}" accept-charset="UTF-8">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    <div class="main-form">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name" class="text-title-field required">Tên sản phẩm</label>
                                <input class="form-control" placeholder="Nhập tên" data-counter="120" name="name"
                                       type="text" id="name" value="{{$data->name}}">
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label">Mô tả</label>
                                <div class="clearfix"></div>
                                <textarea class="form-control form-control " rows="2"
                                          placeholder="Mô tả ngắn" data-counter="1000" id="description"
                                          name="description" cols="50" style="">{!! $data->description !!}</textarea>
                                <small class="charcounter">(1000
                                    kí tự còn lại)</small>
                            </div>
                            <div class="form-group">
                                <label for="content" class="text-title-field">Chi tiết sản phẩm</label>
                                <div class="clearfix"></div>
                                {!! Form::editor('contents',$data->content) !!}

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4>
                                <span> Hình ảnh</span>
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="product-images-wrapper">
                                <a href="#" class="add-new-product-image btn_select_gallery js-btn-trigger-add-image"
                                   data-name="images[]">Thêm
                                    hình ảnh
                                </a>
                                <div class="images-wrapper">
                                    @if($data->image)
                                        <ul class="list-unstyled list-gallery-media-images clearfix  ui-sortable"
                                            style="padding-top: 20px;">
                                            <li class="product-image-item-handler">
                                                <div class="list-photo-hover-overlay">
                                                    <ul class="photo-overlay-actions">
                                                        <li>
                                                            <a class="mr10 btn-trigger-remove-product-image" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete image">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="custom-image-box image-box">
                                                    <input type="hidden" name="images[]" value="{{$data->image}}" class="image-data">
                                                    <img src="{{ get_object_image($data->image, Arr::get($data->image, 'allow_thumb', true) == true ? 'thumb' : null) }}"
                                                         alt="{{ __('preview image') }}" class="preview_image" @if (Arr::get($data->image, 'allow_thumb', true)) width="150"
                                                        @endif>
                                                </div>
                                            </li>
                                        </ul>
                                    @else
                                        <div data-name="images[]"
                                             class="text-center cursor-pointer btn_select_gallery js-btn-trigger-add-image default-placeholder-product-image ">
                                            <img src="https://hasa.botble.com/vendor/core/core/base/images/placeholder.png"
                                                 alt="Hình ảnh" width="120">
                                            <br>
                                            <p style="color:#c3cfd8">Using button <strong>Select image</strong> to add more
                                                images.</p>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="main-manage-product-type">
                        <div class="widget meta-boxes">
                            <div class="widget-title">
                                <h4>
                                    <span> Tổng quan</span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="row price-group">
                                    <input type="hidden" value="0" class="detect-schedule hidden" name="sale_type">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="text-title-field">Mã sản phẩm</label>
                                            <input class="next-input" id="sku" data-counter="30" name="sku" type="text"
                                                   value="{{$data->sku}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-title-field">Giá cơ bản</label>
                                            <div class="next-input--stylized">
                                                <span class="next-input-add-on next-input__add-on--before">$</span>
                                                <input name="price"
                                                       class="next-input input-mask-number regular-price next-input--invisible"
                                                       step="any" value="{{$data->price}}" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-3 right-sidebar">
                    <div class="widget meta-boxes form-actions form-actions-default action-horizontal">
                        <div class="widget-title">
                            <h4>
                                <span>Xuất bản</span>
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="btn-set">
                                <button type="submit" name="submit" value="save" class="btn btn-info">
                                    <i class="fa fa-save"></i> Lưu
                                </button>
                                &nbsp;
                                <button type="submit" name="submit" value="apply" class="btn btn-success">
                                    <i class="fa fa-check-circle"></i> Lưu &amp; chỉnh sửa
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="waypoint"></div>
                    <div class="form-actions form-actions-fixed-top hidden">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="https://hasa.botble.com/admin">Bảng điều khiển</a></li>
                            <li class="breadcrumb-item"><a href="https://hasa.botble.com/admin/ecommerce/products">Thương
                                    mại điện tử</a></li>
                            <li class="breadcrumb-item active">Thêm sản phẩm mới</li>
                        </ol>
                        <div class="btn-set">
                            <button type="submit" name="submit" value="save" class="btn btn-info">
                                <i class="fa fa-save"></i> Lưu
                            </button>
                            &nbsp;
                            <button type="submit" name="submit" value="apply" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> Lưu &amp; chỉnh sửa
                            </button>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="status" class="control-label required" aria-required="true">Trạng
                                    thái</label></h4>
                        </div>
                        <div class="widget-body">
                            <div class="ui-select-wrapper">
                                <select class="form-control ui-select ui-select is-valid" id="status" name="status"
                                        aria-invalid="false">
                                    <option value="published">Published</option>
                                    <option value="draft">Bản nháp</option>
                                    <option value="pending">Đang chờ xử lý</option>
                                </select>
                                <svg class="svg-next-icon svg-next-icon-size-16">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="is_featured" class="control-label">Nổi bật?</label></h4>
                        </div>
                        <div class="widget-body">
                            <div class="onoffswitch">
                                <input type="hidden" name="is_featured" value="0">
                                <input type="checkbox" name="is_featured" class="onoffswitch-checkbox" id="is_featured"
                                       value="1">
                                <label class="onoffswitch-label" for="is_featured">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="categories" class="control-label">Danh mục</label></h4>
                        </div>
                        <div class="widget-body">
                            <div class="form-group form-group-no-margin ">
                                <div
                                    class="multi-choices-widget list-item-checkbox mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar"
                                    style="position: relative; overflow: visible; padding: 0px;">
                                    <div id="mCSB_1"
                                         class="mCustomScrollBox "
                                         style="max-height: 320px;" tabindex="0">
                                        <div id="mCSB_1_container"
                                             class="mCSB_container "
                                             style="position: relative; top: 0px; left: 0px; width: 213.75px;"
                                             dir="ltr">
                                            <ul>@foreach($categories as $category)
                                                    <li>
                                                        <input type="checkbox" class="styled"
                                                               name="product_categories[]"
                                                               @if(in_array($category->id,json_decode($data->categories))) checked
                                                               @endif
                                                               value="{{$category->id}}">
                                                        <label for="product_collections">{{$category->name}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="mCSB_1_scrollbar_vertical"
                                         class="mCSB_scrollTools mCSB_1_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical"
                                         style="display: none;">
                                        <div class="mCSB_draggerContainer">
                                            <div id="mCSB_1_dragger_vertical" class="mCSB_dragger"
                                                 style="position: absolute; min-height: 50px; height: 0px; top: 0px;"
                                                 oncontextmenu="return false;">
                                                <div class="mCSB_dragger_bar" style="line-height: 50px;"></div>
                                                <div class="mCSB_draggerRail"></div>
                                            </div>
                                            <div id="mCSB_1_scrollbar_horizontal"
                                                 class="mCSB_scrollTools mCSB_1_scrollbar mCS-minimal-dark mCSB_scrollTools_horizontal"
                                                 style="display: none;">
                                                <div class="mCSB_draggerContainer">
                                                    <div id="mCSB_1_dragger_horizontal" class="mCSB_dragger"
                                                         style="position: absolute; min-width: 50px; width: 0px; left: 0px;"
                                                         oncontextmenu="return false;">
                                                        <div class="mCSB_dragger_bar"></div>
                                                        <div class="mCSB_draggerRail"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget meta-boxes">
                        <div class="widget-title">
                            <h4><label for="product_collections[]" class="control-label">Nhóm sản phẩm</label></h4>
                        </div>
                        <div class="widget-body">
                            <div class="form-group form-group-no-margin ">
                                <div
                                    class="multi-choices-widget list-item-checkbox mCustomScrollbar _mCS_2 mCS-autoHide mCS_no_scrollbar"
                                    style="position: relative; overflow: visible; padding: 0px;">
                                    <div id="mCSB_2"
                                         class="mCustomScrollBox mCS-minimal-dark mCSB_vertical_horizontal mCSB_outside"
                                         style="max-height: 320px;" tabindex="0">
                                        <div id="mCSB_2_container" class="mCSB_container "
                                             style="position: relative; top: 0px; left: 0px; width: 213.75px;"
                                             dir="ltr">
                                            <ul>
                                                @foreach($collection as $item)
                                                    <li>
                                                        <input type="checkbox" class="styled"
                                                               name="product_collections[]"
                                                               value="{{$item->id}}"
                                                               @if(in_array($item->id,json_decode($data->collections))) checked @endif
                                                        <label for="product_collections">{{$item->name}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="mCSB_2_scrollbar_vertical"
                                         class="mCSB_scrollTools mCSB_2_scrollbar "
                                         style="display: none;">
                                        <div class="mCSB_draggerContainer">
                                            <div id="mCSB_2_dragger_vertical" class="mCSB_dragger"
                                                 style="position: absolute; min-height: 50px; height: 0px; top: 0px;"
                                                 oncontextmenu="return false;">
                                                <div class="mCSB_dragger_bar" style="line-height: 50px;"></div>
                                                <div class="mCSB_draggerRail"></div>
                                            </div>
                                            <div id="mCSB_2_scrollbar_horizontal"
                                                 class="mCSB_scrollTools mCSB_2_scrollbar mCS-minimal-dark mCSB_scrollTools_horizontal"
                                                 style="display: none;">
                                                <div class="mCSB_draggerContainer">
                                                    <div id="mCSB_2_dragger_horizontal" class="mCSB_dragger"
                                                         style="position: absolute; min-width: 50px; width: 0px; left: 0px;"
                                                         oncontextmenu="return false;">
                                                        <div class="mCSB_dragger_bar"></div>
                                                        <div class="mCSB_draggerRail"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
                <div class="col-9">
                    @include('products.variation')
                </div>
        </div>
    </div>
    <div class="row" id="photo-gallery-item">

    </div>

@stop
<!-- end Modal -->

@push('footer')

    <script>
        "use strict";
        $(document).ready(function () {
            $(window).on("load", (function () {
                $("body").on("click", ".list-gallery-media-images .btn_remove_image", (function (e) {
                    e.preventDefault(), $(e.currentTarget).closest("li").remove()
                })), $(document).on("click", ".btn-trigger-remove-product-image", (function (e) {
                    e.preventDefault(), $(e.currentTarget).closest(".product-image-item-handler").remove(), 0 === $(".list-gallery-media-images").find(".product-image-item-handler").length && $(".default-placeholder-product-image").removeClass("hidden")
                })), $(document).on("click", ".btn-trigger-select-product-attributes", (function (e) {
                    e.preventDefault(), $("#store-related-attributes-button").data("target", $(e.currentTarget).data("target")), $("#select-attribute-sets-modal").modal("show")
                })), $(document).on("click", ".btn-trigger-add-new-product-variation", (function (e) {
                    e.preventDefault(), $("#store-product-variation-button").data("target", $(e.currentTarget).data("target")), $("#add-new-product-variation-modal").modal("show")
                })), $(document).on("click", ".btn-trigger-generate-all-versions", (function (e) {
                    e.preventDefault(), $("#generate-all-versions-button").data("target", $(e.currentTarget).data("target")), $("#generate-all-versions-modal").modal("show")
                })), $(document).on("click", ".btn-trigger-add-attribute", (function (e) {
                    e.preventDefault(), $(".list-product-attribute-wrap").toggleClass("hidden"), $(e.currentTarget).toggleClass("adding_attribute_enable"), $(e.currentTarget).hasClass("adding_attribute_enable") ? $("#is_added_attributes").val(1) : $("#is_added_attributes").val(0);
                    var t = $(e.currentTarget).data("toggle-text");
                    $(e.currentTarget).data("toggle-text", $(e.currentTarget).text()), $(e.currentTarget).text(t)
                })), $(document).on("change", ".product-select-attribute-item", (function () {
                    var e = [];
                    $.each($(".product-select-attribute-item"), (function (t, a) {
                        "" !== $(a).val() && e.push(t)
                    })), e.length ? $(".btn-trigger-add-attribute-to-simple-product").removeClass("hidden") : $(".btn-trigger-add-attribute-to-simple-product").addClass("hidden")
                }));

                let e = function () {
                    $.each($(".product-select-attribute-item option"), (function (e, t) {
                        $(t).prop("value") !== $(t).closest("select").val() && (0 === $(".list-product-attribute-wrap-detail .product-select-attribute-item-value-id-" + $(t).prop("value")).length ? $(t).prop("disabled", !1) : $(t).prop("disabled", !0))
                    }))
                };
                $(document).on("change", ".product-select-attribute-item", (function (t) {
                    console.log(t)
                    $(t.currentTarget).closest(".product-attribute-set-item")
                        .find(".product-select-attribute-item-value-wrap")
                        .html($(".list-product-attribute-values-wrap .product-select-attribute-item-value-wrap-" + $(t.currentTarget).val()).html()),
                        $(t.currentTarget).closest(".product-attribute-set-item")
                            .find(".product-select-attribute-item-value-id-" + $(t.currentTarget).val())
                            .prop("name", "added_attributes[" + $(t.currentTarget).val() + "]"), e()
                })), $(document).on("click", ".btn-trigger-add-attribute-item", (function (t) {
                    t.preventDefault();
                    var a = $(".list-product-attribute-values-wrap .product-select-attribute-item-template"), r = null;
                    $.each($(" .product-select-attribute-item option"), (function (e, t) {
                        $(t).prop("value") !== $(t).closest("select").val() && !1 === $(t).prop("disabled") && (a.find(".product-select-attribute-item-value-wrap").html($(".list-product-attribute-values-wrap .product-select-attribute-item-value-wrap-" + $(t).prop("value")).html()), r = $(t).prop("value"))
                    }));
                    var n = $(".list-product-attribute-wrap-detail");
                    n.append(a.html()), n.find(".product-attribute-set-item:last-child .product-select-attribute-item").val(r), n.find(".product-select-attribute-item-value-id-" + r).prop("name", "added_attributes[" + r + "]"), n.find(".product-attribute-set-item").length === $(".list-product-attribute-values-wrap .product-select-attribute-item-wrap-template").length && $(t.currentTarget).addClass("hidden"), $(".product-set-item-delete-action").removeClass("hidden"), e()
                })), $(document).on("click", ".product-set-item-delete-action a", (function (t) {
                    t.preventDefault(), $(t.currentTarget).closest(".product-attribute-set-item").remove();
                    var a = $(".list-product-attribute-wrap-detail");
                    a.find(".product-attribute-set-item").length < 2 && $(".product-set-item-delete-action").addClass("hidden"), a.find(".product-attribute-set-item").length < $(".list-product-attribute-values-wrap .product-select-attribute-item-wrap-template").length && $(".btn-trigger-add-attribute-item").removeClass("hidden"), e()
                })), new RvMediaStandAlone(".images-wrapper .btn-trigger-edit-product-image", {
                    onSelectFiles: function (e, t) {
                        var a = _.first(e), r = t.closest(".product-image-item-handler").find(".image-box"),
                            n = t.closest(".list-gallery-media-images");
                        r.find(".image-data").val(a.url), r.find(".preview_image").attr("src", a.thumb).show(), _.forEach(e, (function (e, t) {
                            if (t) {
                                var a = $(document).find("#product_select_image_template").html().replace(/__name__/gi, r.find(".image-data").attr("name")),
                                    o = $('<li class="product-image-item-handler">' + a + "</li>");
                                o.find(".image-data").val(e.url), o.find(".preview_image").attr("src", e.thumb).show(), n.append(o)
                            }
                        }))
                    }
                }), $(document).on("click", ".list-search-data .selectable-item", (function (e) {
                    e.preventDefault();
                    var t = $(e.currentTarget), a = t.closest(".form-group").find("input[type=hidden]"),
                        r = a.val().split(",");
                    if ($.each(r, (function (e, t) {
                        r[e] = parseInt(t)
                    })), $.inArray(t.data("id"), r) < 0) {
                        a.val() ? a.val(a.val() + "," + t.data("id")) : a.val(t.data("id"));
                        let n = $(document).find("#selected_product_list_template").html().replace(/__name__/gi, t.data("name")).replace(/__id__/gi, t.data("id")).replace(/__url__/gi, t.data("url")).replace(/__image__/gi, t.data("image")).replace(/__attributes__/gi, t.find("a span").text());
                        t.closest(".form-group").find(".list-selected-products").removeClass("hidden"), t.closest(".form-group").find(".list-selected-products table tbody").append(n)
                    }
                    t.closest(".panel").addClass("hidden")
                })), $(document).on("click", ".textbox-advancesearch", (function (e) {
                    let t = $(e.currentTarget), a = t.closest(".box-search-advance").find(".panel");
                    a.removeClass("hidden"), a.addClass("active"), 0 === a.find(".panel-body").length && (Botble.blockUI({
                        target: a,
                        iconOnly: !0,
                        overlayColor: "none"
                    }), $.ajax({
                        url: t.data("target"), type: "GET", success: function (e) {
                            e.error ? Botble.showError(e.message) : (a.html(e.data), Botble.unblockUI(a))
                        }, error: function (e) {
                            Botble.handleError(e), Botble.unblockUI(a)
                        }
                    }))
                })), $(document).on("keyup", ".textbox-advancesearch", (function (e) {
                    let t = $(e.currentTarget), a = t.closest(".box-search-advance").find(".panel");
                    setTimeout((function () {
                        Botble.blockUI({
                            target: a,
                            iconOnly: !0,
                            overlayColor: "none"
                        }), $.ajax({
                            url: t.data("target") + "?keyword=" + t.val(), type: "GET", success: function (e) {
                                e.error ? Botble.showError(e.message) : (a.html(e.data), Botble.unblockUI(a))
                            }, error: function (e) {
                                Botble.handleError(e), Botble.unblockUI(a)
                            }
                        })
                    }), 500)
                })), $(document).on("click", ".box-search-advance .page-link", (function (e) {
                    e.preventDefault();
                    let t = $(e.currentTarget);
                    if (!t.closest(".page-item").hasClass("disabled") && t.prop("href")) {
                        let a = t.closest(".box-search-advance").find(".panel");
                        Botble.blockUI({
                            target: a,
                            iconOnly: !0,
                            overlayColor: "none"
                        }), $.ajax({
                            url: t.prop("href") + "&keyword=" + t.val(), type: "GET", success: function (e) {
                                e.error ? Botble.showError(e.message) : (a.html(e.data), Botble.unblockUI(a))
                            }, error: function (e) {
                                Botble.handleError(e), Botble.unblockUI(a)
                            }
                        })
                    }
                })), $(document).on("click", "body", (function (e) {
                    let t = $(".box-search-advance");
                    t.is(e.target) || 0 !== t.has(e.target).length || t.find(".panel").addClass("hidden")
                })), $(document).on("click", ".btn-trigger-remove-selected-product", (function (e) {
                    e.preventDefault();
                    let t = $(e.currentTarget).closest(".form-group").find("input[type=hidden]"),
                        a = t.val().split(",");
                    $.each(a, (function (e, t) {
                        t = t.trim(), _.isEmpty(t) || (a[e] = parseInt(t))
                    }));
                    let r = a.indexOf($(e.currentTarget).data("id"));
                    r > -1 && a.splice(r, 1), t.val(a.join(",")), $(e.currentTarget).closest("tbody").find("tr").length < 2 && $(e.currentTarget).closest(".list-selected-products").addClass("hidden"), $(e.currentTarget).closest("tr").remove()
                }));
                let t;
                (t = $(".wrap-relation-product")).length && (Botble.blockUI({
                    target: t,
                    iconOnly: !0,
                    overlayColor: "none"
                }), $.ajax({
                    url: t.data("target"), type: "GET", success: function (e) {
                        e.error ? Botble.showError(e.message) : (t.html(e.data), Botble.unblockUI(t))
                    }, error: function (e) {
                        Botble.handleError(e), Botble.unblockUI(t)
                    }
                }))
            }))
            $(".product-select-attribute-item").change(function () {
                $.ajax({
                    url: "{{ route('product.attribute.list.index') }}",
                    method: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": $(this).val()
                    }
                }).done(function (data) {
                    $('.product-select-attribute-item-value').html(``)
                    data.data.forEach(function (item, index) {
                        $('.product-select-attribute-item-value').append(`<option value="${item.id}">${item.name}</option>`)
                    });
                })
            }).change();
            $('.btn_select_gallery').rvMedia({
                onSelectFiles: function (files) {
                    $.each(files, function (index, file) {
                        $('.list-gallery-media-images').append(`
                            <li class="product-image-item-handler">
                                <div class="list-photo-hover-overlay">
                                <ul class="photo-overlay-actions">
                                    <li>
                                        <a class="mr10 btn-trigger-remove-product-image" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete image">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="custom-image-box image-box">
                                <input type="hidden" name="images[]" value="${file.url}" class="image-data">
                                <img src="${file.thumb}" alt="preview image" class="preview_image">
                            </div>
                            </li>
                        `);
                    });

                    $('.default-placeholder-product-image  ').addClass('hidden');
                    $('.list-gallery-media-images ').removeClass('hidden');
                }
            });
            $('.add_image_list ').rvMedia({
                onSelectFiles: function (files) {
                    $.each(files, function (index, file) {
                        $('.variation-images .list-unstyled').append(`
                            <li class="product-image-item-handler">
                                <div class="list-photo-hover-overlay">
                                <ul class="photo-overlay-actions">
                                    <li>
                                        <a class="mr10 btn-trigger-remove-product-image" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete image">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="custom-image-box image-box">
                                <input type="hidden" name="images[]" value="${file.url}" class="image-data">
                                <img src="${file.thumb}" alt="preview image" class="preview_image">
                            </div>
                            </li>
                        `);
                    });

                    $('.variation-images .default-placeholder-product-image  ').addClass('hidden');
                    $('.variation-images .list-gallery-media-images ').removeClass('hidden');
                }
            });
            $('.list-gallery-media-images ').on('click', '.product-image-item-handler', function (e) {
                e.preventDefault();
                $(this).remove();
            });
            $('.btn-trigger-edit-product-version').click(function (t){
                     t.preventDefault(),
                    $("#update-product-variation-button").data("target", $(t.currentTarget).data("target"));
                let a = $(t.currentTarget);
                $.ajax({
                    url: a.data("load-form"), type: "GET", beforeSend: function () {
                        a.addClass("button-loading")
                    }, success: function (res) {

                        res.error ? Botble.showError(res.message) : ($("#edit-product-variation-modal .modal-body").html(res.data),
                            Botble.initResources(), $("#edit-product-variation-modal").modal("show")),
                            a.removeClass("button-loading"),
                            console.log(a.data("load-edit-post"))
                            $("#edit_variation_product").attr('action', a.data("load-edit-post"));
                    }, complete: function () {
                        a.removeClass("button-loading")
                    }, error: function (e) {
                        a.removeClass("button-loading"), Botble.handleError(e)
                    }
                })
            })



        });
    </script>
    <style>
        ul.list-gallery-media-images > li:first-child {
            width: 17.4%;
        }

        ul.list-gallery-media-images > li {
            float: left;
            width: 8.5%;
            border: 1px solid #e4e4e4;
            margin: 2px;
            cursor: move;
        }

        .add-new-product-attribute-wrap .btn-trigger-add-attribute {
            position: absolute;
            right: 10px;
            top: -42px;
        }

        .add-new-product-attribute-wrap {
            position: relative;
        }
        .variation-actions{
            position: absolute;
            right: 18px;
            top: 8px;
        }
        #main-manage-product-type{
            position: relative;
        }
        .variation-actions .wrap-img-product img{
            height: 50px;
        }
        .wrap-img-product img{
            height: 50px;
            width: 50px;
            object-fit: cover;
        }
    </style>
@endpush
