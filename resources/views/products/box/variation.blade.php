
<div class="variation-form-wrapper">
    <div class="row">
        @foreach($attributes as $item)
            @if($item->lists)
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label for="attribute-size1"
                               class="text-title-field required">{{$item->name}}</label>
                        <div class="ui-select-wrapper">
                            <select class="ui-select"
                                    id="attribute-size"
                                    name="attribute_{{$item->name}}"
                                    aria-invalid="false">
                                @foreach($item->lists as $list)
                                    <option value="{{$list['name']}}" @if($data->size==$list['name'] || $data->color==$list['name'] ) selected @endif>
                                        {{$list['name']}}
                                    </option>
                                @endforeach
                            </select>
                            <svg
                                class="svg-next-icon svg-next-icon-size-16">
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="#select-chevron"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <div class="row price-group">
        <input type="hidden" value="0"
               class="detect-schedule hidden" name="sale_type">

        <div class="col-md-4">
            <div class="form-group ">
                <label class="text-title-field">Sku</label>
                <input class="next-input"
                       data-counter="30" name="sku" type="text"
                       value="{{$data->sku}}"
                       aria-invalid="false"><small
                    class="charcounter">(21 character(s)
                    remain)</small>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="text-title-field">Price</label>
                <div class="next-input--stylized"><span class="next-input-add-on next-input__add-on--before">$</span>
                    <input name="price" class="next-input input-mask-number regular-price next-input--invisible"
                           step="any" value="{{$data->price}}" type="number">
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="form-group">
        <div class="storehouse-management">
            <div class="mt5">
                <input type="hidden"
                       name="with_storehouse_management"
                       value="0">
                <label><input type="checkbox"
                              class="hrv-checkbox storehouse-management-status"
                              value="1"
                              name="with_storehouse_management"
                              checked=""> With storehouse
                    management</label>
            </div>
        </div>
    </div>
    <div class="storehouse-info ">
        <div class="form-group">
            <label class="text-title-field">Quantity</label>
            <input type="text"
                   class="next-input input-mask-number input-medium"
                   value="13" name="quantity" im-insert="true">
        </div>
        <div class="form-group">
            <label class="text-title-field">
                <input type="hidden"
                       name="allow_checkout_when_out_of_stock"
                       value="0">
                <input type="checkbox"
                       name="allow_checkout_when_out_of_stock"
                       class="hrv-checkbox" value="1">
                &nbsp;Allow customer checkout when this product
                out of stock
            </label>
        </div>
    </div>
    <hr>
    <div class="shipping-management">
        <label class="text-title-field">Shipping</label>
        <div class="row">
            <div class="col-md-3 col-md-6">
                <div class="form-group">
                    <label>Weight (g)</label>
                    <div class="next-input--stylized"><span class="next-input-add-on next-input__add-on--before">g</span>
                        <input type="text"
                               class="next-input input-mask-number next-input--invisible"
                               name="weight" value="{{$data->weight}}"
                               im-insert="true">
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-6">
                <div class="form-group">
                    <label>Length (cm)</label>
                    <div class="next-input--stylized">
                         <span class="next-input-add-on next-input__add-on--before">cm</span>
                        <input type="text"
                               class="next-input input-mask-number next-input--invisible"
                               name="length" value="{{$data->length}}"
                               im-insert="true">
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-6">
                <div class="form-group">
                    <label>Wide (cm)</label>
                    <div class="next-input--stylized">
                    <span class="next-input-add-on next-input__add-on--before">cm</span>
                        <input type="text" class="next-input input-mask-number next-input--invisible"
                               name="wide" value="{{$data->wide}}"
                               im-insert="true">
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-6">
                <div class="form-group">
                    <label>Height (cm)</label>
                    <div class="next-input--stylized">
                         <span class="next-input-add-on next-input__add-on--before">cm</span>
                        <input type="text"
                               class="next-input input-mask-number next-input--invisible"
                               name="height" value="{{$data->height}}"
                               im-insert="true">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="variation-images" style="position: relative; border: 1px dashed #ccc; padding: 10px;">
        <div class="product-images-wrapper">
            <a href="#" class="add-new-product-image js-btn-trigger-add-image" >Edit Image</a>
            <div class="images-wrapper">
                <div data-name="images[]"
                     class="text-center cursor-pointer js-btn-trigger-add-image default-placeholder-product-image  hidden ">
                    <img src="{{asset('placeholder.png')}}"
                        alt="Image" width="120">
                    <br>
                    <p style="color:#c3cfd8">Using button
                        <strong>Select image</strong> to add
                        more images.</p>
                </div>
                <ul class="product_variation_images clearfix ui-sortable"
                    style="padding-top: 20px; list-style-type: none">
                    @if($data->image)
                    <li class="product-image-item-handler">
                        <div class="list-photo-hover-overlay">
                            <ul class="photo-overlay-actions">
                                <li>
                                    <a class="mr10 btn-trigger-remove-product-image" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete image">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="custom-image-box image-box">
                            <input type="hidden" name="images[]" value="{{$data->image}}" class="image-data variation_edit">
                            <img src="{{ get_object_image($data->image, Arr::get($data->image, 'allow_thumb', true) == true ? 'thumb' : null) }}"
                                 alt="{{ __('preview image') }}" class="preview_image" @if (Arr::get($data->image, 'allow_thumb', true)) width="150"
                                @endif>

                        </div>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $('.add-new-product-image').rvMedia({
        onSelectFiles: function (files) {
            if ($('.product_variation_images img').length) {
                $(".variation_edit").val(files[0].url);
                $(".custom-image-box img").attr("src",files[0].thumb);

            }else {
                $('.product_variation_images').append(`
                            <li class="product-image-item-handler">
                                <div class="list-photo-hover-overlay">
                                <ul class="photo-overlay-actions">
                                    <li>
                                        <a class="mr10 btn-trigger-remove-product-image" data-toggle="tooltip" data-placement="bottom" data-original-title="Delete image">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="custom-image-box image-box">
                                <input type="hidden" name="images[]" value="${files[0].url}" class="image-data">
                                <img src="${files[0].thumb}" alt="preview image" class="preview_image">
                            </div>
                            </li>
                        `);
            }


            $('.default-placeholder-product-image  ').addClass('hidden');
            $('.list-gallery-media-images ').removeClass('hidden');
        }
    });
</script>
<style>

    ul.product_variation_images > li:first-child{
        width: 17.4%;
        position: relative;

    }

    ul.product_variation_images> li {
        float: left;
        width: 8.5%;
        border: 1px solid #e4e4e4;
        margin: 2px;
        cursor: move;
    }

    .add-new-product-attribute-wrap .btn-trigger-add-attribute {
        position: absolute;
        right: 10px;
        top: -42px;
    }
    .product_variation_images >li:hover .list-photo-hover-overlay{opacity:1}

</style>

