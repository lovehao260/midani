
@foreach ($menus = dashboard_menu()->getAll() as $menu)
    @if($menu['id']=='cms-core-appearance')
        <li class="nav-item has-ul @if ($menu['active']===url('')) active @endif " id="cms-plugins-product">
            <a href="{{url('/admin/ecommerce')}}" class="nav-link nav-toggle">
                <i class="fa fa-edit"></i>
                <span class="title">Thương mại điện tử </span>
                <span class="arrow"></span>         </a>
            <ul class="sub-menu  hidden-ul " style="display: none;">
                <li class="nav-item " id="cms-plugins-blog-post">
                    <a href="{{url('/admin/ecommerce/products')}}" class="nav-link">
                        <i class=""></i>
                        Sản phẩm
                    </a>
                </li>
                <li class="nav-item " id="cms-plugins-blog-categories">
                    <a href="{{route('product.attribute.index')}}" class="nav-link">
                        <i class=""></i>
                        Thuộc tính sản phẩm
                    </a>
                </li>
                <li class="nav-item " id="cms-plugins-blog-tags">
                    <a href="{{url('admin/ecommerce/product-categories')}}" class="nav-link">
                        <i class=""></i>
                        Danh mục sản phẩm
                    </a>
                </li>
                <li class="nav-item " id="cms-plugins-blog-tags">
                    <a href="{{url('admin/ecommerce/product-collections')}}" class="nav-link">
                        <i class=""></i>
                        Nhóm sản phẩm
                    </a>
                </li>
            </ul>
        </li>
    @endif
    <li class="nav-item @if ($menu['active']) active @endif" id="{{ $menu['id'] }}">
        <a href="{{ $menu['url'] }}" class="nav-link nav-toggle">
            <i class="{{ $menu['icon'] }}"></i>
            <span class="title">{{ trans($menu['name']) }} {!! apply_filters(BASE_FILTER_APPEND_MENU_NAME, null, $menu['id']) !!}</span>
            @if (isset($menu['children']) && count($menu['children'])) <span class="arrow @if ($menu['active']) open @endif"></span> @endif
        </a>
        @if (isset($menu['children']) && count($menu['children']))
            <ul class="sub-menu @if (!$menu['active']) hidden-ul @endif">
                @foreach ($menu['children'] as $item)
                    <li class="nav-item @if ($item['active']) active @endif" id="{{ $item['id'] }}">
                        <a href="{{ $item['url'] }}" class="nav-link">
                            <i class="{{ $item['icon'] }}"></i>
                            {{ trans($item['name']) }}
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </li>
@endforeach
<script>

</script>

