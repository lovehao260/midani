<div class="page whitePage viewIn pageLoaded" view-controller="ProductsListing"
     post-link="https://portaromana.com/products/search/">
    <div class="productHeader">
        <div class="cont">
            @if(isset($category))
                <h1>{{$category->name  }} </h1>
            @else
                <h1>Product</h1>
            @endif
            <ul class="filterTitles">
                <li data-term="colour">Colour</li>
                <li data-term="material">Material</li>
                <li data-term="size">Size</li>
                <li data-term="style">Style</li>
                <div class="clearFilters">Clear filters</div>
            </ul>
            <a href="{{url('products')}}" class="backTo">Back to Products</a>
        </div>
        <div class="filterSet" data-term="colour">
            <div class="cont">
                <ul>
                    <li data-term-id="2889">Antique Gold</li>
                    <li data-term-id="6">Gold</li>
                    <li data-term-id="459">Clear</li>
                    <li data-term-id="8">Pink</li>
                    <li data-term-id="9">Silver</li>
                    <li data-term-id="466">Natural</li>
                    <li data-term-id="7">Blue</li>
                    <li data-term-id="469">Black</li>
                    <li data-term-id="472">Yellow</li>
                    <li data-term-id="478">Purple</li>
                    <li data-term-id="10">Dark</li>
                    <li data-term-id="470">Red/Orange</li>
                    <li data-term-id="481">All</li>
                    <li data-term-id="455">Light/White</li>
                    <li data-term-id="473">Green</li>
                </ul>
            </div>
        </div>
        <div class="filterSet" data-term="material">
            <div class="cont">
                <ul>
                    <li data-term-id="11">Brass</li>
                    <li data-term-id="12">Glass</li>
                    <li data-term-id="456">Sculpted</li>
                    <li data-term-id="460">Glass/Perspex</li>
                    <li data-term-id="462">Painted</li>
                    <li data-term-id="463">Metal</li>
                    <li data-term-id="464">Wooden</li>
                    <li data-term-id="467">Ceramic</li>
                    <li data-term-id="477">Upholstered</li>
                    <li data-term-id="2764">ALL</li>
                    <li data-term-id="2855">Flocked</li>
                    <li data-term-id="2997">Cane</li>
                    <li data-term-id="2998">Cast Composite</li>
                    <li data-term-id="3000">Hyacinth</li>
                    <li data-term-id="3001">Stone</li>
                </ul>
            </div>
        </div>
        <div class="filterSet" data-term="size">
            <div class="cont">
                <ul>
                    <li data-term-id="16">Small</li>
                    <li data-term-id="17">Medium</li>
                    <li data-term-id="18">Large</li>
                    <li data-term-id="476">0-39cm</li>
                    <li data-term-id="474">40-59cm</li>
                    <li data-term-id="475">60-79cm</li>
                    <li data-term-id="468">80-99cm</li>
                    <li data-term-id="480">100cm+</li>
                </ul>
            </div>
        </div>
        <div class="filterSet" data-term="style">
            <div class="cont">
                <ul>
                    <li data-term-id="19">Modern</li>
                    <li data-term-id="20">Classic</li>
                    <li data-term-id="452">Simple</li>
                    <li data-term-id="453">Contemporary</li>
                    <li data-term-id="457">Organic</li>
                    <li data-term-id="458">Statement</li>
                    <li data-term-id="461">Sculptural</li>
                    <li data-term-id="465">Whimsical</li>
                    <li data-term-id="471">Colour</li>
                    <li data-term-id="479">Tribal</li>
                    <li data-term-id="2766">Metal</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="products cont ">
        <div class="noProducts">
            <h3>No products found</h3>
        </div>
        <div class="productListing colsContainer">
            <div class="cols cols-4 equalHeights productGrid colHasMargins flexGrid originalProductGrid">
                @foreach($products as $product)

                    <div class="col product">
                        <a href="{{route('public.product.detail',$product->slug)}}">
                            <div class="imageBlock productImage equalHeightEl productPortrait isLoaded">
                                <img src="{{ get_object_image($product['image'], Arr::get($product['image'], 'allow_thumb', true) == true ? 'thumb' : null) }}"
                                     alt="{{ __('preview image') }}" class="preview_image" >
                            </div>
                        </a>
                        <div class="productInformation">
                            <a href="https://portaromana.com/product/blob-lamp-2/"><h2>{{$product->name}}</h2>
                                <h3>Amethyst</h3>
                                <p class="productCode">{{$product->sku}}</p>
                            </a>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
</div>
<style>
    .products{
        margin-top: 70px;
    }
</style>
