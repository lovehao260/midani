<footer class="page-footer bg-dark pt-50">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row  ">
            <div class="col-12 col-md-3 ">
                <aside class="widget widget--transparent widget__footer widget__about">
                    <div class="widget__header">
                        <h3 class="widget__title">Thông Tin</h3>
                    </div>
                    <div class="widget__content">
                        <p class="person-info__description">{{ theme_option('site_description') }}</p>
                        <div class="person-detail">
                            <p><i class="fa fa-home"></i>{{ theme_option('address') }}</p>
                            <p><i class="fa fa-globe"></i><a
                                    href="{{ theme_option('website') }}">{{ theme_option('website') }}</a></p>
                            <p><i class="fa fa-envelope"></i><a
                                    href="mailto:{{ theme_option('contact_email') }}">{{ theme_option('contact_email') }}</a>
                            </p>
                        </div>
                    </div>
                </aside>
            </div>

            <div class="col-12 col-md-3 ">
                <div class="widget widget--transparent widget__footer">
                    <div class="widget__header">
                        <h3 class="widget__title">Bài Viết</h3>
                    </div>
                    <div class="widget__content">

                        <ul class="list list--light list--fadeIn">
                            @foreach(get_recent_posts(5) as $post)
                            <li>
                                <a href="{{ $post->url }}" data-number-line="2">{{$post->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 ">
                <aside class="widget widget--transparent widget__footer">
                    <div class="widget__header">
                        <h3 class="widget__title">Danh Mục</h3>
                    </div>
                    <div class="widget__content">
                        <ul class="list list--fadeIn list--light">
                            {!!
                                 Menu::renderMenuLocation('main-menu', [
                                     'options' => ['class' => 'menu sub-menu--slideLeft'],
                                     'view'    => 'main-menu',
                                 ])
                             !!}
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="col-12 col-md-3 ">
                <div class="fb-page" data-href="https://www.facebook.com/huuphitrieuledinh/" data-tabs="timeline"
                     data-width="" data-height="300"  data-small-header="false" data-adapt-container-width="true"
                     data-hide-cover="false" data-show-facepile="true" >

                </div>
            </div>
        </div>
    </div>
    <div id="page-footer-bottom">
        <div class="container">
            <div class="row cols ">
                <div class="col  col-3-4">
                    <div class="page-copyright">
                        <p>{{setting('theme-midani-seo_title')}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="page-footer__social">
                        <ul class="social social--simple">
                            <li>
                                <a href="{{ theme_option('facebook') }}" title="Facebook"><i
                                        class="hi-icon fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="{{ theme_option('twitter') }}" title="Twitter"><i
                                        class="hi-icon fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href=" {{ theme_option('youtube') }}" title="Youtube"><i
                                        class="hi-icon fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="back2top"><i class="fa fa-angle-up"></i></div>
</div>
<style>
    .bg-overlay {
        content: "";
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        opacity: .9;
        background-color: #000;
    }

    .page-footer {
        position: relative;
    }

    @media screen and (min-width: 1024px) {
        .page-footer .container {
            width: 80%;
            margin-left: 10%;
        }
    }

    ,


    .social {
        position: relative;
        z-index: 10;
        display: inline-block;
    }

    .social li {
        float: left;
        margin-right: 15px;
        position: relative;
    }

    ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    .social--simple li a {
        display: inline-block;
        color: #fff;
        width: 25px;
        height: 25px;
        background-color: #9d9d9d;
        border-radius: 3px;
    }

    .social--simple li a i {
        position: absolute;
        top: 45%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 10;
    }

    }
    ul {
        list-style: none;
        padding: 0;
        margin: 0
    }

    p:empty {
        margin-bottom: 0
    }

    .list a {
        display: inline-block;
        font-size: 13px;
        color: #666
    }

    .list li {
        position: relative;
        margin-bottom: 10px;
        transition: all .4s ease
    }

    .list li:hover a {
        color: #ed3723;
    }

    .list--fadeIn li:before {
        content: "\F105";
        display: inline-block;
        position: absolute;
        top: 50%;
        left: 0;
        font-family: FontAwesome;
        color: #ed3723;
        visibility: hidden;
        opacity: 0;
        transform: translateY(-50%);
        transition: all .4s ease
    }

    .list--fadeIn li:hover {
        padding-left: 15px
    }

    .list--fadeIn li:hover:before {
        visibility: visible;
        opacity: 1
    }

    #back2top {
        position: fixed;
        bottom: 0;
        right: 40px;
        width: 40px;
        height: 40px;
        z-index: 10000;
        background-color: transparent;
        border-radius: 50%;
        border: 2px solid #ed3723;
        transition: all .5s ease;
        visibility: hidden;
        opacity: 0
    }

    #back2top i {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        color: #ed3723;
        z-index: 10001;
        font-size: 20px
    }

    #back2top:hover {
        cursor: pointer;
        background-color: #ed3723;
    }

    #back2top:hover i {
        color: #fff
    }

    #back2top.active {
        bottom: 90px;
        visibility: visible;
        opacity: 1
    }

    .widget {
        margin-bottom: 30px;
        background-color: #fff;
        box-shadow: 0 0 7px rgba(0, 0, 0, .1)
    }

    .widget .widget__content, .widget .widget__header {
        padding: 15px
    }

    .widget .widget__header {
        background-color: #f9f9f9
    }

    .widget .widget__title {
        font-size: 20px;
        font-weight: 600;
        color: #363636;
        margin-bottom: 0
    }

    .widget__recent-post .post {
        margin-bottom: 15px;
        padding-bottom: 15px;
        border-bottom: 1px dashed #eee
    }

    .widget__recent-post li:last-child .post {
        border-bottom-width: 0
    }

    .widget__about p {
        font-size: 13px;
        color: #666;
        line-height: 1.6em;
        padding-right: 20px;
    }

    .widget__about .person-detail p {
        color: #e4e4e4;
        margin-bottom: 15px;
        font-size: 13px
    }

    .widget__about .person-detail i {
        margin-right: 10px;
        font-size: 20px;
        vertical-align: middle;
        color: #ed3723;
    }

    .widget__about .person-detail a {
        color: #fff
    }

    .widget__about .person-detail a:hover {
        color: #ed3723;
    }

    .widget--transparent {
        background-color: transparent;
        box-shadow: none
    }

    .widget--transparent .widget__content, .widget--transparent .widget__header {
        padding: 0
    }

    .widget--transparent .widget__header {
        position: relative;
        margin-bottom: 20px;
        padding-bottom: 5px;
        border: none;
        background-color: transparent;
        padding-top: 25px;
    }

    .widget--transparent .widget__header:after {
        content: "";
        position: absolute;
        top: 100%;
        left: 0;
        width: 25px;
        height: 2px;
        background-color: #ed3723;
        z-index: 10
    }

    .widget--transparent .post {
        margin-bottom: 0
    }

    .widget__footer .widget__title {
        text-transform: uppercase;
        font-size: 14px;
        color: #e4e4e4
    }

    .widget__tags .tag-link {
        display: inline-block;
        padding: 5px 15px;
        margin-right: 10px;
        margin-bottom: 10px;
        background-color: #ecf0f1;
        color: #666;
        font-size: 12px
    }

    .widget__tags .tag-link:last-child {
        margin-right: 0
    }

    .widget__tags .tag-link:hover {
        background-color: #ed3723;
        color: #fff
    }

    .widget__tags--transparent .tag-link {
        background-color: #fff
    }

    .page-intro {
        position: relative;
        text-align: center
    }

    .page-intro:after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(26, 35, 126, .3);
        z-index: 0
    }

    .page-intro .page-intro__title {
        position: relative;
        z-index: 10;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: .1em;
        font-size: 24px;
        margin-bottom: 20px
    }

    .page-intro .social {
        margin-top: 10px
    }

    .page-intro .breadcrumb {
        position: relative;
        z-index: 10;
        background-color: transparent;
        margin-bottom: 0
    }

    .page-intro .breadcrumb li {
        color: #e4e4e4;
        font-size: 13px;
        padding: 20px 0 5px;
    }

    .page-intro .breadcrumb a:hover, .page-intro .breadcrumb li:active {
        color: #ed3723;
    }

    #page-footer-bottom {
        position: relative;
        padding: 10px 0 5px;
        text-align: left;
        background: #000;
    }

    .page-footer__social {
        padding-top: 1em;
    }
</style>
<!-- JS Library-->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
{!! Theme::footer() !!}
@if (session()->has('success_msg'))
    <script type="text/javascript">
        swal('{{ __('Success') }}', "{{ session('success_msg', '') }}", 'success');
    </script>
@endif

@if (session()->has('error_msg'))
    <script type="text/javascript">
        swal('{{ __('Success') }}', "{{ session('error_msg', '') }}", 'error');
    </script>
@endif

<script type="text/javascript" src="{{asset('themes/midani/js/index.js')}}">
</script>
<script>


  /* calling currentTime() function to initiate the process */
    $('.mobileMenuToggle').click(function (){
        $('.navbar-nav').toggle()
    })
    if (window.screen.width >= 1024 ) {
        if (window.location.href.split('/').pop() === "") {
            $('header .logo').addClass('logo-home')
            $(window).scroll(function (event) {
                let scroll = $(window).scrollTop();
                if (scroll < 20){
                    $('header .logo').addClass('logo-home')
                }else {
                    $('header .logo').removeClass('logo-home')
                }
            });
        }
    }


</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId={{ setting('facebook_app_id', config('plugins.facebook.general.app_id')) }}&autoLogAppEvents=1"></script>

<div class="fb-customerchat"
     attribution=setup_tool
     page_id="157007981299897"
     theme_color="#0084ff">
</div>

</body>
</html>
