<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>{{ SeoHelper::getTitle() }}</title>

    <!-- Fonts-->
    <link href="https://fonts.googleapis.com/css?family={{ theme_option('primary_font', 'Roboto') }}" rel="stylesheet"
          type="text/css">
    <!-- CSS Library-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{asset('/themes/midani/css/index.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('themes/midani/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('/themes/midani/css/style.css')}}" rel="stylesheet" type="text/css">
    <style>
        body {
            font-family: '{{ theme_option('primary_font', 'Roboto') }}', sans-serif !important;
        }
    </style>
    <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
</head>
<!--[if IE 7]>
<body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 8]>
<body class="ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 9]>
<body class="ie9 lt-ie10"><![endif]-->
<body class="isSet index">
<header class=" ">
    <div class="cont">
        <a href="https://portaromana.com" class="logo">
            <span class="logo">
                MIDANE
            </span>
        </a>

        {!!
               Menu::renderMenuLocation('main-menu', [
                   'options' => ['class' => 'nav navbar-nav mobileMenuScreen'],
                   'view' => 'main-menu',
               ])
           !!}

        <ul class="topRightNav">
            <li>
                <a href="#">
                    <div class="icon search doSearchOverlay">
                        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="44" height="44"
                             viewBox="0 0 44 44"><title>search_white</title>
                            <path
                                d="M27.34,12.52a10.12,10.12,0,1,0-1.76,15.74l5.32,5.32,3.16-3.16-5.32-5.38A10.12,10.12,0,0,0,27.34,12.52Zm-2.22,12.1a7,7,0,1,1,0-9.87A7,7,0,0,1,25.12,24.62Z"
                                style="fill:#fff"></path>
                            <rect width="44" height="44" style="fill:none"></rect>
                        </svg>
                    </div>
                </a></li>
        </ul>
        <div class="mobileMenuToggle">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

</header>

<div class="searchOverlay">
    <div class="close">
        <div class="close-icon-container">
  <span class="close-icon">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
      <line x1="15" y1="15" x2="25" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round"
            stroke-miterlimit="10"></line>
      <line x1="25" y1="15" x2="15" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round"
            stroke-miterlimit="10"></line>
    </svg>
  </span>
        </div>
    </div>
    <div class="tbl">
        <div class="tblCell">
            <div class="searchBar">
                <form class="quick-search" action="{{ route('product.search') }}" enctype="multipart/form-data">
                    <input type="text" name="q" placeholder="Nhập sản phẩm hoặc tin tức cần tìm..." class="form-control search-input"
                           autocomplete="off">
                    <input class="searchButton" type="submit" value="Tìm Kiếm">
                </form>
                <div class="searchResults">
                    <h4>Suggestions:</h4>
                    <ul></ul>
                </div>
            </div>
        </div>
    </div>
</div>
