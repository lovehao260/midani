<?php

namespace Theme\Ripple\Http\Controllers;

use App\Model\Product;
use App\Model\ProductAttribute;
use App\Model\ProductAttributeList;
use App\Model\ProductCategory;
use App\Model\ProductCollection;
use App\Model\ProductCollectionImage;
use App\Model\ProductImage;

use Botble\Base\Http\Responses\BaseHttpResponse;

use Botble\Blog\Repositories\Interfaces\PostInterface;
use Botble\Blog\Repositories\Interfaces\TagInterface;
use Botble\Slug\Repositories\Interfaces\SlugInterface;
use SeoHelper;

use Botble\Theme\Http\Controllers\PublicController;
use Illuminate\Http\Request;
use Theme;

class RippleController extends PublicController
{
    /**
     * @var TagInterface
     */
    protected $tagRepository;

    /**
     * @var SlugInterface
     */
    protected $slugRepository;

    /**
     * PublicController constructor.
     * @param TagInterface $tagRepository
     * @param SlugInterface $slugRepository
     */
    public function __construct(TagInterface $tagRepository, SlugInterface $slugRepository)
    {
        $this->tagRepository = $tagRepository;
        $this->slugRepository = $slugRepository;
    }

    /**
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getIndex(BaseHttpResponse $response)
    {
        $product_categories=ProductCategory::all();
        return Theme::scope('index',['product_categories'=>$product_categories])->render();
    }


    public function getProducts (BaseHttpResponse $response, $key = null){
        $product_categories=ProductCategory::all();
        return Theme::scope('product',['product_categories'=>$product_categories])->render();
    }


    public function getProductsAll(){
        $products=Product::all();
        return Theme::scope('product_list',['products'=>$products])->render();
    }

    public function getProductsCategory ($slug){
        $category = ProductCategory::where('slug', $slug)->first();
        $product_att=ProductAttribute::all();
        foreach ($product_att as $att){
            $att->lists=ProductAttributeList::where('parent_id',$att->id)->get()->toArray();
        }

        //Get users in project
        $products=Product::all();
        $array=[];
        foreach ($products as $product){
            if (in_array($category->id, \GuzzleHttp\json_decode($product->categories))) {
                array_push ($array,$product);
            }
        }
        return Theme::scope('product_list',['products'=>$array,'category'=>$category,'product_att'=>$product_att])->render();

    }
    public function getProductsCollections ($slug){
        $collect = ProductCollection::where('slug', $slug)->first();
        $product_att=ProductAttribute::all();
        foreach ($product_att as $att){
            $att->lists=ProductAttributeList::where('parent_id',$att->id)->get()->toArray();
        }
        //Get users in project
        $products=Product::all();
        $array=[];
        foreach ($products as $product){
            if (in_array($collect->id, \GuzzleHttp\json_decode($product->collections))) {
                array_push ($array,$product);
            }
        }
        return Theme::scope('product_list',['products'=>$array,'product_att'=>$product_att])->render();

    }
    public function getProductsDetail($slug){
        $product = Product::where('slug', $slug)->first();
        $product_images=ProductImage::where('product_id',$product->id)->get()->toArray();
        $product_variations=ProductImage::where('product_id',$product->id)->get()->toArray();

        $products_att=Product::where( 'sku', $product->sku . '-%')->get();

        $product_collection=ProductCollection::findOrFail(json_decode($product->collections)[0]);

        return Theme::scope('product_detail',['product'=>$product,'product_variations'=>$product_variations,
            'product_images'=>$product_images,'product_collection'=>$product_collection,
            'products_att'=>$products_att])->render();
    }
    /**
     * @param BaseHttpResponse $response
     * @param null $key
     * @return BaseHttpResponse|\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getView(BaseHttpResponse $response, $key = null)
    {
        return parent::getView($response, $key);
    }

    /**
     * @return mixed
     */
    public function getSiteMap()
    {

        return parent::getSiteMap();
    }

    public function getCollections(){
        $product_collections=ProductCollection::all();
        return Theme::scope('collection',['product_collections'=>$product_collections])->render();
    }

    public function getCollectionDetail($slug){
        $collections = ProductCollection::where('slug', $slug)->first();
        $collection_all=ProductCollection::where('slug','<>',$slug)->get();
        $array= ProductCollectionImage::where('collection_id',$collections->id)->get();

        return Theme::scope('collection_detail',['array'=>$array,'collections'=>$collections,'collection_all'=>$collection_all])->render();
    }

    public function getSearchProducts(Request $request,PostInterface $postRepository){

        $query = $request->q;
        SeoHelper::setTitle(__('Search result for: ') . '"' . $query . '"')
            ->setDescription(__('Search result for: ') . '"' . $query . '"');
        $products = Product::where('name', 'LIKE', '%' . $query . '%')->get();
        $posts = $postRepository->getSearch($query, 0, 12);


        Theme::breadcrumb()
            ->add(__('Home'), url('/'))
            ->add(__('Search result for: ') . '"' . $query . '"', route('product.search'));

        return Theme::scope('search',compact('posts','products'))->render();
    }
}
