<?php



Route::group(['namespace' => 'Theme\Ripple\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(apply_filters(BASE_FILTER_GROUP_PUBLIC_ROUTE, []), function () {

        Route::get('/', 'RippleController@getIndex')->name('public.index');
        Route::get('/products', 'RippleController@getProducts')->name('public.product');
        Route::get('/products/all', 'RippleController@getProductsAll')->name('public.product.all');
        Route::get('/product-category/{slug}', 'RippleController@getProductsCategory')->name('public.product.categories');
        Route::get('/products/{slug}', 'RippleController@getProductsDetail')->name('public.product.detail');
        Route::get('/search', 'RippleController@getSearchProducts')->name('product.search');

        Route::get('/collections', 'RippleController@getCollections')->name('public.collection');
        Route::get('/collections/{slug}', 'RippleController@getCollectionDetail')->name('public.collection.detail');
        Route::get('/collections/{slug}/products', 'RippleController@getProductsCollections')->name('public.collection.products');
        Route::get('sitemap.xml', [
            'as'   => 'public.sitemap',
            'uses' => 'RippleController@getSiteMap',
        ]);

        Route::get('{slug?}' . config('core.base.general.public_single_ending_url'), [
            'as'   => 'public.single',
            'uses' => 'RippleController@getView',
        ]);

    });

});
