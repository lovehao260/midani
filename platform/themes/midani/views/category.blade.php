<div id="container">
    <div class="text-dark page-blog viewIn pageLoaded" view-controller="NewsEvents">

        <section class="newsPosts">
            <div class="cont newsEventsHeader">
                <div class="sectionIntro text-center">
                    <h3>{{  $category->name}}</h3>
                    <div class="textContent">
                        TIN TỨC VÀ SỰ KIỆN MỚI NHẤT TỪ Midani
                    </div>
                </div>
                <div class="newsLeft">
                    Việt Nam
                    <div class="clockCurrentTime">
                        <div id="clock"></div>
                    </div>
                </div>

                <div class="newsRight">
                    <ul class="archiveFilters d-flex">
                        <li class="pl-5">
                            <select name="categories" autocomplete="off" class="selectLinkUpdate"
                                    onchange="handleSelect(this.value)">
                                @foreach($categories as $categorie)
                                    <option
                                        value="{{url(Str::slug($categorie['name'], '-'))}}">{{$categorie['name']}}</option>
                                @endforeach
                            </select>
                        </li>
                    </ul>
                    <div class="page-sidebar">
                        {!! Theme::partial('sidebar') !!}
                    </div>
                </div>
            </div>
            <div class="cont">
                <div class="colsContainer">
                    <div class="row ">
                        @if ($posts->count() > 0)
                            @foreach ($posts as $post)
                                <div class="col-md-4 col-12 newsThumb">
                                    <a href="{{ $post->url }}" title="{{ $post->name }}">
                                        <div class="imageBlock newsThumb equalHeightEl isLoaded">
                                            <img src="{{ get_image_url($post->image, 'new') }}"
                                                 style="background-image: url('{{ get_object_image($post->image) }}');"
                                                 alt="{{ $post->name }}">
                                        </div>
                                    </a>
                                    <div class="newsInfo textContent">
                                        <a href="{{ url('new-events') }}">
                                            <div class="meta">{{ date_from_database($post->created_at, 'M d, Y') }} •&nbsp;Tin
                                                Tức, Bài Viết
                                            </div>
                                            <h2>     {{ $post->name }}</h2></a>
                                        <p>    {{ $post->description }}</p>
                                        <a href="{{ $post->url }}">Xem Thêm</a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="no-post text-center w-100">
                                <h3  class="no-post text-center">{{ __('Không Có Bài Viết Nào!') }}</h3>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </section>

        <section class="social galleryScope">
            <div class="cont">
                <div class="sectionIntro">
                    <h3>Inside Porta Romana</h3>
                    <div class="textContent">
                        Join us on all our social outlets and keep up-to-date with all our news
                        <div class="socialLinks">
                            <a href="{{ theme_option('facebook') }}" title="Facebook" class="hi-icon fa fa-facebook"></a>
                            <a href="{{ theme_option('twitter') }}" title="Twitter" class="hi-icon fa fa-google-plus"></a>
                            <a href="{{ theme_option('youtube') }}" title="Youtube" class="hi-icon fa fa-youtube"></a>
                        </div>

                    </div>
                </div>
                <div class="colsContainer ">
                    <div class=" colHasMargins equalHeights row">
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'/storage/new-images/new-image1.jpg'}}'); background-size: cover;">
                                <img src="{{'/storage/new-images/new-image1.jpg'}}"  alt="image new 1">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-2.jpg'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-2.jpg'}}"  alt="image new 2">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-3.png'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-3.png'}}"  alt="image new 3">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 colPadded  " >
                            <div class="textOverlay">
                                <div class="tbl">
                                    <div class="tblCell">
                                        <div class="textContent">
                                            <h5>Follow us</h5>
                                            <div class="">
                                                <a href="{{ theme_option('facebook') }}" title="Facebook" class="hi-icon fa fa-facebook"></a>
                                                <a href="{{ theme_option('twitter') }}" title="Twitter" class="hi-icon fa fa-google-plus"></a>
                                                <a href="{{ theme_option('youtube') }}" title="Youtube" class="hi-icon fa fa-youtube"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row colHasMargins equalHeights">
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-4.jpg'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-4.jpg'}}"  alt="image new 6">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-5.jpg'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-5.jpg'}}"  alt="image new 5">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-6.jpg'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-6.jpg'}}"  alt="image new 6">
                            </div>
                        </div>

                        <div class="col-12 col-md-3 colPadded    ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock isLoaded"
                                 style="background-image: url('{{'storage/new-images/new-image-7.jpg'}}'); background-size: cover;">
                                <img src="{{'storage/new-images/new-image-7.jpg'}}" alt="image new 7">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<style>
    .page-blog{
        margin: 80px ;
    }
    .imageBlock img{
        width: 100%;
    }
     .page-blog .textOverlay {
        background-color: #000;
        color: #666;
        text-align: center;
       left: 15px;
       margin-right: -15px;
    }
    .newsSocial .textOverlay h5{
        font-size: 16px;
    }
    .newsSocial .textOverlay a{
        padding: 5px;
    }
    .page-blog .colHasMargins{
        margin-top: 30px;
    }
     section.social {
        border-top: 1px solid #e0e0e0;
        padding-top: 54px;
        padding-bottom: 54px;
        text-align: center;
    }
    .sectionIntro .textContent {
        max-width: 440px;
        margin: 0 auto;
        font-family: 'Gotham SSm A', 'Gotham SSm B', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-style: normal;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 1.5px;
        text-transform: uppercase;
    }
    .page-blog h3{
        color: black;
    }
    .socialLinks a{
        padding: 5px;
    }
    @media screen and (max-width: 640px) {
        .page-blog {
            margin: 15px;
        }
        .sectionIntro{
            padding-top: 50px;
        }
        .page-blog .textOverlay{
            height: 300px;
            position: relative;
            left: 0;
        }
    }
</style>
<script>
    function currentTime() {
        var date = new Date(); /* creating object of Date class */
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        hour = updateTime(hour);
        min = updateTime(min);
        sec = updateTime(sec);
        document.getElementById("clock").innerText = hour + " : " + min + " : " + sec; /* adding time to the div */
        var t = setTimeout(function () {
            currentTime()
        }, 1000); /* setting timer */
    }

    function updateTime(k) {
        if (k < 10) {
            return "0" + k;
        } else {
            return k;
        }
    }
    currentTime();
</script>

