<div class=" category-product ">
    <div class="productHeader">
        <div class="cont  ">
            @if(isset($category))
            <h1>{{$category->name  }} </h1>
            @else
                <h1>Product</h1>
            @endif
            <ul class="filterTitles">
                @foreach($product_att as $att)
                <li data-term="{{$att->name}}">{{$att->name}}</li>
                @endforeach
                <div class="clearFilters">Clear filters</div>
            </ul>
            <a href="{{url('products')}}" class="backTo">Back to Products</a>
        </div>
        @foreach($product_att as $att)
            <div class="filterSet" data-term-item="{{$att->name}}">
                <ul>
                    @foreach($att->lists as $list)
                        <li data-term-id="{{$list['id']}}" class="checkbox" onclick="fillterProduct(this)">{{$list['name']}}</li>
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
    <section class="section pt-100 pb-50 bg-lightgray products">
        <div class="row colsContainer">
                    @if (count($products) > 0)
                        @foreach($products as $product)
                        <div class="col-12 col-md-3 equalHeights newsGrid colHasMargins">
                            <div class="col newsThumb">
                                <a href="{{route('public.product.detail',$product->slug)}}">
                                    <div class="imageBlock newsThumb equalHeightEl isLoaded">
                                        <img src="{{ get_image_url($product['image']) }}"
                                             alt="{{ $product['$product'] }}">
                                    </div>
                                </a>
                                <div class="productInformation">
                                    <a href="{{route('public.product.detail',$product->slug)}}"><h5>{{$product->name}}</h5>
                                        <h6>Amethyst</h6>
                                        <p class="productCode">{{$product->sku}}</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="no-post">
                            <h3 class="no-post text-center">{{ __('Không Có Bài Viết Nào!') }}</h3>
                        </div>
                    @endif


            </div>
    </section>
</div>
<style>
    .category-product {
        padding-top: 80px;
    }

    .page {
        min-height: unset;
        margin-top: 70px;
    }

    .new-search-title {
        color: #000;
        text-shadow: 2px 2px 2px #6c757d;
        text-align: center;
        padding-bottom: 30px;
    }

    .text-center {
        text-align: center;
    }
    .widget__about .person-info__thumbnail {
        width: 60px;
        float: left;
    }
    .imageBlock.isLoaded img{
        height: 365px;
    }
    .productInformation{
        text-align: center;
        padding-top: 15px;
        font-size: 14px;
        color: #4b4b4b;
        margin-bottom: 5px;
    }
    .products img{
        object-fit: contain;
    }

</style>
