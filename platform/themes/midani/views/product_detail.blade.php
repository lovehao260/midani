<div class="page whitePage post_product viewIn pageLoaded" view-controller="Product">

    <div class="tooltip" style="">
        <span>Click to view larger</span>
    </div>
    <div class="productHeader">
        <div class="cont">
            <ul>
                <li dropdown-target="colourways"><span>Alternative Colours</span></li>
                <li dropdown-target="moreinformation"><span>Request more information</span></li>
                <li><a href="#" class="shareButton">Share</a></li>
            </ul>
        </div>

    </div>
    <section class="product cont colsContainer">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="productInfo">
                    <div class="productTopInfo">
                        <h5>{{$product->name}}</h5>

                        <h3 class="detail">Nickel | {{$product->sku}}</h3>
                    </div>
                    <div class="productFeatures">
                        <h6>Mô tả</h6>
                        <div class="textContent">
                            {{$product->description}}
                        </div>
                    </div>
                    <div class="productDetails">
                        <h4>Details</h4>
                        <ul>
                            {!! $product->content !!}
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-8 product-slider">
                <div class=" productGallery">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner slider-product">
                            <div class="carousel-item active">
                                <img class=" " src="{{asset('storage/'.$product->image)}}">
                            </div>
                            @foreach($product_images as $product_image)
                                <div class="carousel-item">
                                    <img src="{{asset('storage/'.$product_image['name'])}}" alt="{{$product_image['name']}}">
                                </div>
                            @endforeach

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="collections ">
        <div class=" equalHeights row">
            <div class="col-12 col-md-4 col-sm-6 colPadded colPadded1">
                <div class="imageBlock isLoaded">
                    <img src="{{asset('storage/'.$product_collection->image)}}" alt="{{$product_collection->image}}">
                </div>
            </div>
            <div class="col-12 col-md-4 col-sm-12 colTextLead colTextLeadDark">
                <div class="tbl">
                    <div class="tblCell text-center p-md-5 p-3">
                        <div class="textContent p-md-5">
                            <h2 class="p-sm-3">{{$product_collection->name}}</h2>
                            <p >{{$product_collection->description}}</p>
                        </div>
                        <a href="{{route('public.collection.products',['slug'=>$product_collection->slug])}}" class="button">Xem Thêm sản phẩm</a></div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-sm-6 colPadded">
                <div class="halfCol">
                    <div class="imageBlock isLoaded">
                        <img src="{{asset('themes/midani/images/products-collection1.jpg')}}">
                    </div>
                </div>
                <div class="halfCol">
                    <a href="Craftsmanship">
                        <div class="imageBlock isLoaded">
                            <img src="{{asset('themes/midani/images/product-collection2.jpg')}}">
                        </div>

                        <div class="gradOverlay"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="relatedProducts">
        <div class="products cont colsContainer">
            <h3 class="title">SẢN PHẨM LIÊN QUAN</h3>
            <div class="productListing row">
                <div class="col-md-4 col-12 equalHeights productGrid colHasMargins">
                    @foreach($products_att as $product)
                        <div class="col product">
                            <a href="{{route('public.product.detail',$product->slug)}}">
                                <div class="imageBlock productImage equalHeightEl productPortrait isLoaded">
                                    <img src="{{ get_object_image($product['image'], Arr::get($product['image'], 'allow_thumb', true) == true ? 'thumb' : null) }}"
                                         alt="{{ __('preview image') }}" class="preview_image" >
                                </div>
                            </a>
                            <div class="productInformation">
                                <a href="{{route('public.product.detail',$product->slug)}}"><h2>{{$product->name}}</h2>
                                    <p class="productCode">{{$product->sku}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>

<style>
    section.product {
        background-color: #fff;
        color: #000;
        padding-top: 130px;
        padding-bottom: 50px;
    }
    section.product .productInfo {
        padding-left: 80px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        max-width: 440px;
    }
    section.product .productInfo .productTopInfo {
        padding-bottom: 53px;
    }
    section.product .productInfo .productTopInfo h1 {
        font-family: 'Gotham SSm A','Gotham SSm B','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-weight: 400;
        font-style: normal;
        font-size: 16px;
        line-height: 18px;
        letter-spacing: 2px;
    }
    section.product .productInfo .productTopInfo h3.detail {
        font-family: 'Gotham SSm A','Gotham SSm B','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-weight: 400;
        font-style: normal;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 1.5px;
        text-transform: uppercase;
        color: #666;
    }
    section.product .productInfo .productFeatures{
        color: #666;
        padding-bottom: 14px;
    }
        section.product .productInfo .productDetails h4 {
        font-family: 'Gotham SSm A','Gotham SSm B','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-weight: 400;
        font-style: normal;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 1.5px;
        text-transform: uppercase;
        color: #000;
        padding-bottom: 10px;
    }
    section.product .productInfo .productDetails {
        padding-top: 20px;
        padding-bottom: 20px;
    }
    section.product .productInfo .productDetails {
        padding-top: 15px;
        border-top: 1px solid #d8d8d8;
    }
    .productGallery{
        height: 600px;
        position: relative;
    }
    .galleryScope .carousel-control-prev{
        color: #0D0A0A;
    }
    .carousel-control-prev-icon,
    .carousel-control-next-icon {
        position: absolute;
        left: 0;
        top: 50%;
        z-index: 20;
        cursor: pointer;
        opacity: .3;
        -webkit-transition: -webkit-transform .8s ease,opacity .8s ease;
        -moz-transition: -moz-transform .8s ease,opacity .8s ease;
        -ms-transition: -ms-transform .8s ease,opacity .8s ease;
        -o-transition: -o-transform .8s ease,opacity .8s ease;
        transition: transform .8s ease,opacity .8s ease;
        width: 50px;
        height: 50px;
        margin-top: -25px;
    }

    .carousel-control-prev-icon:after
    {
        content: ' ';
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        width: 22px;
        height: 1px;
        background-color: #000;
        transform: translate3d(0,-8px,0) rotate(-45deg);

    }
    .carousel-control-prev-icon:before
    {
        transform: translate3d(0,7px,0) rotate(45deg);
        content: ' ';
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        width: 22px;
        height: 1px;
        background-color: #000;
    }
    .carousel-control-next-icon:after {
        content: ' ';
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        width: 22px;
        height: 1px;
        background-color: #000;
        transform: translate3d(0,7px,0) rotate(-45deg);
    }
    .carousel-control-next-icon:before {
        content: ' ';
        display: block;
        position: absolute;
        left: 0;
        top: 50%;
        width: 22px;
        height: 1px;
        background-color: #000;
        transform: translate3d(0,-8px,0) rotate(45deg);
    }
    #carouselExampleIndicators{
        height: 600px;
    }
    #carouselExampleIndicators .carousel-indicators li{
        background-color: rgba(0,0,0,0.2);
        margin-top: 50px;
    }
    #carouselExampleIndicators .carousel-indicators  .active{
        background-color: #000;
    }
    #carouselExampleIndicators .carousel-indicators{
        bottom: -15px;
    }
    #carouselExampleIndicators .carousel-item{
        height: 600px;
    }
    #carouselExampleIndicators .carousel-item img{
        object-fit: contain;
    }
    .slider-product {
        position: absolute;
        height: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 35%;
    }

     .halfCol {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 50%;
    }
    .colTextLeadDark .button {
        background-color: #fff;
        color: #000;
        padding: 12px 24px;
    }
    @media (min-width: 768px) and (max-width: 1024px) {
        section.product .productInfo{
            padding-left: 50px;
        }
        .slider-product{
            width: 50%;
        }
        .colTextLeadDark{
            order: -1;
            max-width: 100%;
            flex: none;
        }
        .colPadded{
            max-width: 50%;
            flex: unset;
        }
        .colPadded1{
            padding-right: 0;
        }
    }
    @media screen and (max-width: 640px) {
         .colTextLead{
             order: -1;
             background: #0D0A0A;
            min-height: 60px;
         }
        .product-slider{
            order: -1;
        }
        .productGallery{
            height: 300px;
            order: -1;
        }
        #carouselExampleIndicators{
            height: 300px;
            padding-bottom: 50px;
        }
        #carouselExampleIndicators .carousel-item{
            height: 300px;
        }
        .productInfo{
            padding-top: 50px;
            padding-left: 15px !important;
        }
        .carousel-control-prev-icon{
            left: 30px;
        }
        .halfCol{
            position: relative;
            height: unset;
        }
    }

</style>
