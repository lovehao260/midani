<div class="page viewIn pageLoaded" view-controller="ProductsOverview">
    <div class="colsContainers">
        <ul class="flex-container">
            <li class="flex-item">
                <div class="cols cols-1 categories-item equalHeights">
                    <div class="col colPadded colTextLead colTextLeadDark">
                        <div class="textOverlay">
                            <div class="tbl">
                                <div class="tblCell">
                                    <div class="textContent">
                                        <h2>Sản Phẩm</h2>
                                        <p class="p1"><span class="s1">Kim loại điêu khắc và đúc, thủy tinh thổi chuyên nghiệp,
                                                gốm tráng men thủ công và nhựa đúc thủ công đều có vai trò trong dòng sản phẩm
                                                của chúng tôi. Mỗi thiết kế, được tạo ra và phát triển trong nhà, sau đó sẽ được
                                                trao cho các Nhà sản xuất đáng kinh ngạc của chúng tôi, những người biến ý tưởng
                                                của chúng tôi thành những tác phẩm kỳ diệu hữu hình.
                                                Sản phẩm của chúng tôi là trái tim của mọi thứ chúng tôi làm.</span>
                                        </p>
                                    </div>
                                    <div class="buttons">
                                        <a href="{{url('collections')}}"
                                           class="button"><span>Xem tất cả nhóm sản phẩm</span></a>
                                        <a href="{{url('products/all')}}" class="button"><span>Xem tất cả sản phẩm</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @foreach($product_categories as $category)
                <li class="flex-item">
                    <div class="categories-item ">
                        <a href="{{route('public.product.categories',$category->slug)}}">
                            <div class="colPadded ">
                                <div class="textOverlay">
                                    <div class="footerText text-center">
                                        <div class="textContent">
                                            <h3>{{$category->name}}</h3>
                                            <p>View {{$category->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="gradOverlay"></div>
                                <div class="imageBlock  isLoaded">
                                    <img src="{{asset('storage/'.$category->image)}}">
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
        @endforeach
    </div>
</div>
<style>
    .cols .col.colTextLead.colTextLeadDark {
        background-color: #000;
        color: #fff;
        text-align: center;
    }

    .cols .col.colPadded:before {
        content: ' ';
        width: 100%;
        display: block;
    }

    .tbl .tblCell {
        display: table-cell;
        vertical-align: middle;
    }

    .cols .col.colTextLead .textContent {
        margin: 0 auto;
        width: 80%;
        max-width: 440px;
        padding: 40px 20px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .col.colTextLead .textContent h2 {
        padding-bottom: 32px;

    }

    .cols .col.colTextLead .buttons {
        position: absolute;
        bottom: 44px;
        left: 0;
        width: 100%;
    }

    .col.colTextLead .buttons .button {
        display: block;
        width: 100%;
        position: relative;
        bottom: auto;
        left: auto;
        -webkit-transform: translateX(0);
        -moz-transform: translateX(0);
        -ms-transform: translateX(0);
        -o-transform: translateX(0);
        transform: translateX(0);
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        background-color: transparent;
        padding-top: 9px;
        padding-bottom: 8px;
    }

    .flex-container {
        display: flex;
        flex-flow: row wrap;
        justify-content: space-around;
        padding: 0;
        margin: 0;
        list-style: none;
        width: 100%;
    }

    .flex-item {
        width: 30%;
        margin-top: 10px;
        position: relative;
    }

    .flex-item:nth-child(2) {
        width: 70%;

    }

    .flex-item:nth-child(3) {
        width: 70%;
    }

    .flex-item:nth-child(4) {
        width: 30%;
    }

    .flex-item:nth-child(5) {
        width: 35%;
    }

    .flex-item:nth-child(6) {
        width: 65%;
    }

    .flex-item img {
        width: 100%;
        height: 100%;
        max-height: 780px;
        object-fit: cover;
    }

    .colsContainers {
        margin-top: 41px;
    }

    .colTextLeadDark, .categories-item, .colPadded, .imageBlock {
        height: 100%;
    }

    .textOverlay .footerText {
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 40px 20px;
    }
</style>


