
<section class="search-product pt-100 pb-50 bg-lightgray">
    <h1 class="new-search-title">Sản Phẩm</h1>
    <div class="cont" view-controller="Product">
        <div class="colsContainer">
            <div class="row equalHeights newsGrid colHasMargins">
                @if (count($products) > 0)
                    @foreach($products as $product)
                        <div class="col-md-3 col-sm-6 col-12 newsThumb">
                            <a href="{{route('public.product.detail',$product->slug)}}">
                                <div class="imageBlock newsThumb equalHeightEl isLoaded">
                                    <img src="{{ get_image_url($product['image']) }}"
                                         alt="{{ $product['$product'] }}">
                                </div>
                            </a>
                            <div class="productInformation">
                                <a href="{{route('public.product.detail',$product->slug)}}"><h2>{{$product->name}}</h2>
                                    <h3>Amethyst</h3>
                                    <p class="productCode">{{$product->sku}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="no-post w-100 text-center">
                        <h3 class="no-post text-center">{{ __('Không Có Bài Viết Nào!') }}</h3>
                    </div>
                @endif
            </div>

        </div>
    </div>
</section>
<section class=" pt-100 pb-50 bg-lightgray">
    <h1 class="new-search-title">Tin Tức Và Sự Kiện</h1>
    <div class="cont">
        <div class="colsContainer">
            <div class="row equalHeights newsGrid colHasMargins">
                @if ($posts->count() > 0)
                    @foreach ($posts as $post)
                        <div class="col-md-4 col-12 newsThumb">
                            <a href="{{ $post->url }}" title="{{ $post->name }}">
                                <div class="imageBlock newsThumb equalHeightEl isLoaded">
                                    <img src="{{ get_object_image($post->image) }}"
                                         style="background-image: url('{{ get_object_image($post->image) }}');"
                                         alt="{{ $post->name }}">
                                </div>
                            </a>
                            <div class="newsInfo textContent">
                                <a href="{{ url('new-events') }}">
                                    <div class="meta">{{ date_from_database($post->created_at, 'M d, Y') }} •&nbsp;Tin
                                        Tức, Bài Viết
                                    </div>
                                    <h2>     {{ $post->name }}</h2></a>
                                <p>    {{ $post->description }}</p>
                                <a href="{{ $post->url }}">Xem Thêm</a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="no-post">
                        <h3 class="no-post text-center">{{ __('Không Có Bài Viết Nào!') }}</h3>
                    </div>
                @endif
            </div>

        </div>
    </div>
</section>
<style>
    .search-product {
        margin-top: 70px;
    }

    .page {
        min-height: unset;
        margin-top: 70px;
    }

    .products {
        color: black;
    }

    .new-search-title {
        color: #000;
        text-shadow: 2px 2px 2px #6c757d;
        text-align: center;
        padding-bottom: 30px;
    }

    .text-center {
        text-align: center;
    }
    .widget__about .person-info__thumbnail {
        width: 60px;
        float: left;
    }
    .productInformation{
        text-align: center;
        padding-top: 15px;
        font-size: 14px;
        color: #4b4b4b;
        margin-bottom: 5px;
    }
    .colsContainer{
        width: 80%;
        margin-left: 10%;
    }
</style>
