@if (is_plugin_active('blog'))
    @php
        $featured = get_featured_posts(6);
        $featuredList = [];
        if (!empty($featured)) {
            $featuredList = $featured->pluck('id')->all();
        }
    @endphp




    <div id="container home-page">
        <div class="page viewIn pageLoaded">
            <div class="row ">
                <div class="imageBlock parallaxEl isLoaded" style="transform: translateY(-15.53px);">
                    <img src="https://portaromana.com/wp-content/uploads/2020/08/test-bg-1-2000x1250.jpg">
                </div>
                <button class="arrow-down"></button>
            </div>
            <div class="text-center w-100 pb-60 home-title">
                <strong>Nơi có </strong><br>
                nội thất đẹp nhất thế giới
            </div>
            <div class="row category-list">
                @foreach($product_categories as $category)
                    <div class="col-md-4 col-12 banner-effect">
                        <a href="{{route('public.product.categories',$category->slug)}}" target="_blank">
                            <div class="imageBlock  isLoaded">
                                <img src=" {{ get_object_image($category->image) }}"
                                     alt="{{ Arr::get($category->image, 'description') }}">
                            </div>
                            <div class="textOverlay">
                                <div class="footerText">
                                    <div class="text-center">
                                        <h3>{{$category->name}}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="gradOverlay"></div>
                        </a>
                    </div>

                @endforeach
            </div>

            <div class="row textRow socialRow p-5">
                <div class="textContent">
                    <h3 class="text-dark">Bên trong Midani SHop</h3>
                    <p>HÃY THAM GIA VỚI CHÚNG TÔI TRÊN TẤT CẢ CÁC ĐẦU RA XÃ HỘI CỦA CHÚNG TÔI VÀ LƯU GIỮ ĐẾN NGÀY VỚI
                        TẤT CẢ CÁC TIN TỨC CỦA CHÚNG TÔI
                    </p>
                    <div class="socialLinks">
                        <a href="{{ theme_option('facebook') }}" title="Facebook" class="hi-icon fa fa-facebook"></a>
                        <a href="{{ theme_option('twitter') }}" title="Twitter" class="hi-icon fa fa-google-plus"></a>
                        <a href="{{ theme_option('youtube') }}" title="Youtube" class="hi-icon fa fa-youtube"></a>
                    </div>
                </div>
            </div>

            <div class=" index-footer">
                <div class="row cols cols-3 ">
                    <div class="col-12 col-md-4  instagramPost">
                        <div class="textOverlay">
                            <div class="footerText">
                                <div class="text-center">
                                    <p><span class="socialIcon icon instagram"></span>@midani
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="imageBlock isLoaded" >
                            <img src="{{asset('storage/new-images/new-image-5.jpg')}}">
                        </div>
                        <div class="gradOverlay"></div>
                    </div>
                    <div class="col-12 col-md-4  instagramPost">
                        <div class="textOverlay">
                            <div class="footerText">
                                <div class="text-center pb-30">
                                    <p><span class="socialIcon icon instagram"></span>@midani
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="imageBlock isLoaded">
                            <img src="{{asset('storage/new-images/new-image-4.jpg')}}">
                        </div>
                        <div class="gradOverlay"></div>
                    </div>
                    <div class="col-12 col-md-4  newsPost">
                        <a href="https://portaromana.com/news/glorious-golds/">
                            <div class="textOverlay">
                                <div class="footerText">
                                    <div class="text-center">
                                        <p>Tin tức và sự kiện</p>
                                    </div>
                                </div>
                            </div>

                            <div class="imageBlock isLoaded">
                                <img src="{{asset('storage/News/n1.jpg')}}">
                            </div>

                        </a>
                        <div class="gradOverlay"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endif

