
<div class="page viewIn pageLoaded" view-controller="ProductsOverview">
    <div class="colsContainers">
        <div class="row titleRow textRow">
            <div class="textContent">
                <h2>Nhóm Sản Phẩm</h2>
                <p>
                    Khám phá bộ sưu tập của chúng tôi
                </p>
            </div>
        </div>
        <ul class="flex-container">
            @foreach($product_collections as $collection)
                <li class="flex-item">
                    <div class="categories-item ">
                        <a href="{{route('public.collection.detail',$collection->slug)}}">
                            <div class="colPadded ">
                                <div class="textOverlay">
                                    <div class="footerText">
                                        <div class="textContent text-center">
                                            <h3>{{$collection->name}}</h3>
                                            <p>Xem {{$collection->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="gradOverlay"></div>
                                <div class="imageBlock  isLoaded">
                                    <img src="{{asset('storage/'.$collection->image)}}">
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
        @endforeach
    </div>
</div>
<style>
    .flex-container {
        display: flex;
        flex-flow: row wrap;
        padding: 0;
        margin: 0;
        list-style: none;
        width: 100%;
    }

    .flex-item {
        width: 33.33333%;
        position: relative;
    }


    .flex-item img {
        width: 100%;
        height: 100%;
    }

    .colsContainers {
        margin-top: 20px;
    }
    .colTextLeadDark, .categories-item, .colPadded, .imageBlock {
        height: 100%;
    }
</style>


