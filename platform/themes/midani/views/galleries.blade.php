<div id="container">
    <div class="viewIn pageLoaded" view-controller="Destinations">
        <div class="tooltip">
            <span>Click to view larger</span>
        </div>
        <div class="headerArea ">
            <h1>Bộ Sưu Tập</h1>
            <div class="row titleRow textRow">
                <div class="textContent">
                    <p>Nội thất thương mại, nhà ở và du thuyền đã cung cấp các giai đoạn lớn nhất cho
                        các sản phẩm</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="destination-gallery row">
                @if (isset($galleries) && !$galleries->isEmpty())
                    @foreach ($galleries as $gallery)
                        <div class="image col-md-4 col-12">
                            <a href="{{ route('public.gallery', $gallery->slug) }}">
                                <img src="{{ get_image_url($gallery->image, 'new') }}" alt="{{ $gallery->name }}">
                                <div class="image-overlay text-white"><h2>{{ $gallery->name }}</h2></div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
</div>

<style>
    .headerArea{

        position: relative;
        padding: 85px 0 30px;
        text-align: center;
        z-index: 5;
    }
    .headerArea h1{
        color: #0D0A0A;
    }
    .col-md-4{
        padding: 15px;
    }
    .destination-gallery img{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
     .image-overlay{
        position: absolute;
        top: 15px;
        right: 15px;
        bottom: 15px;
        left: 15px;
        z-index: 2;
        display: flex;
        align-items: center;
        text-align: center;
        justify-content: center;
        background-image: linear-gradient(180deg,rgba(255,255,255,0.3) 0%,rgba(0,0,0,0.4) 63%,rgba(0,0,0,0.58) 100%);
    }
</style>
