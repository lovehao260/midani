<div id="container" style="margin-top: 50px">
    <div class=" blog-detail viewIn pageLoaded" >
        <div class="cont newsEventsHeader">
            <div class="row titleRow textRow">
                <div class="textContent">
                    <div class="meta">{{ date_from_database($post->created_at, 'M d, Y') }}
                        •&nbsp;{{$post->categories->first()->name }}</div>
                    <h1>{{ $post->name }}</h1>
                    <a href="#" class="shareButton">Share</a></div>
            </div>
            <div class="newsLeft">
                Việt Nam
                <div class="clockCurrentTime">
                    <div id="clock"></div>
                </div>
            </div>
            <div class="newsRight">
                <ul class="archiveFilters d-flex">
                    <li class="pr-3">
                        <select name="categories" autocomplete="off" class="selectLinkUpdate"
                                onchange="handleSelect(this.value)">
                            @foreach($categories as $categorie)
                                <option
                                    value="{{url( Str::slug($categorie['name'], '-'))}}">{{$categorie['name']}}</option>
                            @endforeach
                        </select>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="page-content col-12">
                    <div class="post__content">
                        @if (defined('GALLERY_MODULE_SCREEN_NAME') && !empty($galleries = gallery_meta_data($post)))
                            {!! render_object_gallery($galleries, ($post->categories()->first() ? $post->categories()->first()->name : __('Uncategorized'))) !!}
                        @endif
                        {!! $post->content!!}
                        <div class="fb-like" data-href="{{ Request::url() }}" data-layout="standard" data-action="like"
                             data-show-faces="false" data-share="true"></div>
                    </div>
                    <footer class="post__footer">
                        @foreach (get_related_posts($post->id, 2) as $related_item)
                            <div class="post__relate-group @if ($loop->last) post__relate-group--right @endif">
                                <h4 class="relate__title">@if ($loop->first) {{ __('Previous Post') }} @else {{ __('Next Post') }} @endif</h4>
                                <article class="post post--related">
                                    <div class="post__thumbnail"><a href="{{ $related_item->url }}"
                                                                    class="post__overlay"></a>
                                        <img src="{{ get_image_url($related_item['image'], 'thumb') }}"
                                             alt="{{ $related_item->name }}">
                                    </div>
                                    <div class="post__header"><a
                                            href="{{ $related_item->url }}"
                                            class="post__title"> {{ $related_item->name }}</a></div>
                                </article>
                            </div>

                        @endforeach
                    </footer>
                    <br/>
                    {!! apply_filters(BASE_FILTER_PUBLIC_COMMENT_AREA, Theme::partial('comments')) !!}
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .blog-detail ,.blog-detail h1 ,    .blog-detail  h2{
        color: black;
    }
    .post__footer {
        display: flex;
        margin-top: 20px;
    }

    .post__relate-group {
        flex: 1;
    }
    .post--related .post__header {
        float: left;
        /* left: calc(100% - 331px); */
        padding-left: 15px;
        border-bottom: none;
        position: absolute;
        top: 0;
        left: 157px;
    }
    .relate__title {
        position: relative;
        margin-bottom: 20px;
        padding-bottom: 5px;
        font-size: 12px;
        font-weight: 700;
        color: #333;
    }

    .relate__title:after {
        content: "";
        position: absolute;
        top: 100%;
        left: 0;
        width: 25px;
        height: 2px;
        background-color: #3ca2bb;
        z-index: 10;
    }
    .post--related{
        position: relative;
    }
    .post--related .post__header {
        float: left;
        width: calc(100% - 100px);
        padding-left: 15px;
        border-bottom: none;
    }
    .blog-detail{
        padding: 50px;
    }
    .post__thumbnail img{
        width: 30%;
    }
</style>
<script>
    function currentTime() {
        var date = new Date(); /* creating object of Date class */
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        hour = updateTime(hour);
        min = updateTime(min);
        sec = updateTime(sec);
        document.getElementById("clock").innerText = hour + " : " + min + " : " + sec; /* adding time to the div */
        var t = setTimeout(function () {
            currentTime()
        }, 1000); /* setting timer */
    }

    function updateTime(k) {
        if (k < 10) {
            return "0" + k;
        } else {
            return k;
        }
    }
    currentTime();
</script>
