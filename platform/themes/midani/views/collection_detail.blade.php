<div class="page viewIn pageLoaded" view-controller="ProductsOverview">
    <div class="colsContainers">
        <ul class="flex-container">
            <li class="flex-item">
                <div class="cols cols-1 categories-item equalHeights">
                    <div class="col colPadded colTextLead colTextLeadDark">
                        <div class="textOverlay">
                            <div class="tbl">
                                <div class="tblCell">
                                    <div class="textContent text-center">
                                        <h2>{{$collections->name}}</h2>
                                        <p class="p1"><span class="s1">
                                                {!! $collections->description !!}
                                             </span>
                                        </p>
                                    </div>
                                    <div class="buttons">
                                        <a href="{{url('collections/'.$collections->slug.'/products')}}"
                                           class="button"><span>Xem tất cả sản phẩm</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="flex-item">
                <div class="categories-item ">
                    <div class="colPadded ">
                        <div class="gradOverlay"></div>
                        <div class="imageBlock  isLoaded">
                            <img src="{{asset('storage/'.$collections->image)}}">
                        </div>
                    </div>
                </div>
            </li>
            @foreach($array as $item)
                <li class="flex-item">
                    <div class="categories-item ">
                        <div class="colPadded ">
                            <div class="gradOverlay"></div>
                            <div class="imageBlock  isLoaded">
                                <img src="{{asset('storage/'.$item->image)}}">
                            </div>
                        </div>
                    </div>
                </li>
        @endforeach
    </div>
    <div class="row textRow otherCollections">
        <div class="textContent">
            <h4 class="text-dark pb-3">Khám phá các bộ sưu tập khác</h4>
            <ul>
                @foreach($collection_all as $collection)
                    <a href="{{url('collections/'.$collection->slug)}}">
                        <li>{{$collection->name}}</li>
                    </a>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<style>

    .flex-container {
        display: flex;
        flex-flow: row wrap;

        padding: 0;
        margin: 0;
        list-style: none;
        width: 100%;
    }

    .flex-item {
        width: 33.333%;
        position: relative;
        max-height: 750px;
    }

    .flex-item img {
        object-fit: cover;
    }


    .flex-item img {
        width: 100%;
        height: 100%;
    }

    .colsContainers {
        margin-top: 60px;
    }

    .colTextLeadDark, .categories-item, .colPadded, .imageBlock {
        height: 100%;
    }

    .textContent {
        margin: 0 auto;
        width: 80%;
        max-width: 440px;
        padding: 40px 20px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .cols .col .button {
        display: block;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        background-color: transparent;
        padding-top: 9px;
        padding-bottom: 8px;
        text-align: center;
    }

    .row.textRow {
        padding: 52px 0 54px;
    }

    .row.otherCollections {
        color: #000;
        background-color: #fff;
    }

    .row.otherCollections ul a {
        color: #7f7f7f;
        -webkit-transition: color .8s ease;
        -moz-transition: color .8s ease;
        -ms-transition: color .8s ease;
        -o-transition: color .8s ease;
        transition: color .8s ease;
    }
</style>


