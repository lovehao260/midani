<div class="productListing colsContainer">
    <div class="cols cols-4 equalHeights productGrid colHasMargins flexGrid originalProductGrid">
        @foreach($products as $product)

        <div class="col product">
            <a href="{{route('public.product.detail',$product->slug)}}">
                <div class="imageBlock productImage equalHeightEl productPortrait isLoaded">
                    <img src="{{ get_object_image($product['image'], Arr::get($product['image'], 'allow_thumb', true) == true ? 'thumb' : null) }}"
                         alt="{{ __('preview image') }}" class="preview_image" >
                </div>
            </a>
            <div class="productInformation">
                <a href="https://portaromana.com/product/blob-lamp-2/"><h2>{{$product->name}}</h2>
                    <h3>Amethyst</h3>
                    <p class="productCode">{{$product->sku}}</p>
                </a>
            </div>
        </div>

        @endforeach
    </div>
</div>
