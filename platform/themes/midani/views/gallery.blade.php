<section class="section pt-50 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-content">
                    <article class="post post--single">
                        <div class="post__content">
                            <h1 class="text-dark text-center d-flex ">
                                <div class="gallery-title">
                                        <span>
                                             Bộ sưu tập
                                        </span>
                                    <ul class="drop-down closed">
                                        <li><a href="#" class="nav-button"> {{ $gallery->name }}</a></li>
                                        @foreach($gallery_all as $item)
                                            <li><a href="{{url('/gallery/'.$item->slug)}}">{{$item->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </h1>
                            <p>
                                {{ $gallery->description }}
                            </p>
                            <div id="list-photo">
                                @foreach (gallery_meta_data($gallery) as $image)

                                    @if ($image)
                                        <div class="item" data-src="{{ get_image_url($image['img'], 'new') }} " data-sub-html="{{ Arr::get($image, 'description') }}">
                                            <div class="photo-item">
                                                <div class="thumb">
                                                    <a href="#">
                                                        <img src="{{ get_image_url($image['img'], 'new') }}" alt="{{ Arr::get($image, 'description') }}">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <br>
                            {!! apply_filters(BASE_FILTER_PUBLIC_COMMENT_AREA, Theme::partial('comments')) !!}
                        </div>
                    </article>
                </div>
            </div>

        </div>
    </div>
</section>
<link href="{{asset('/themes/midani/css/lightgallery.min.css')}}" rel="stylesheet" type="text/css">
<style>
    #list-photo {
        width: 100%
    }

    #list-photo .item {
        width: 33.333%;
        margin-bottom: 14px
    }

    #list-photo .item .photo-item {
        padding-left: 7px;
        padding-right: 7px
    }

    #list-photo .item .photo-item div {
        transition: all .25s;
        padding: 5px;
        box-shadow: 0 1px 4px 0 rgba(0, 0, 0, .3)
    }

    #list-photo .item .photo-item div:hover {
        background: rgba(63, 63, 62, .1)
    }

    #list-photo .item .photo-item img {
        border: 1px solid rgba(63, 63, 62, .4);
        display: block;
        width: 100%;
        height: 100%;
    }

    .gallery-wrap .gallery-item {
        width: 32.8%;
        margin-right: .8%;
        float: left;
        max-height: 250px;
        overflow: hidden;
        margin-bottom: 10px;
        position: relative
    }

    .gallery-wrap .gallery-item:nth-child(3n) {
        margin-right: 0
    }

    .gallery-wrap .gallery-item .gallery-detail {
        position: absolute;
        bottom: -50px;
        right: 0;
        left: 0;
        z-index: 2;
        background: rgba(0, 0, 0, .8);
        color: #fff;
        padding: 4px 10px;
        transition: .25s ease
    }

    .gallery-wrap .gallery-item .gallery-detail a {
        color: #fff
    }

    .gallery-wrap .gallery-item .gallery-detail a:hover {
        color: #32c5d2 !important
    }

    .gallery-wrap .gallery-item:hover .gallery-detail {
        bottom: 0
    }

    .gallery-wrap .gallery-item .gallery-detail .gallery-title {
        text-transform: uppercase;
        font-weight: 700
    }

    .gallery-wrap .gallery-item .img-wrap {
        overflow: hidden
    }

    .gallery-wrap .gallery-item .img-wrap img {
        width: 100%
    }
    .lg-outer .lg-thumb{
        position: absolute;
        left: 50%;
        transform: translateX(-50%) !important;
        width: unset;
    }
    .headerArea {
        margin-top: 80px;
    }
    .goGallery img {
        object-fit: cover;
        width: 100%;
        height: 200px !important;
        cursor: pointer;
    }
    .gallery{
        margin: 50px 0;
    }
    .gallery-container .image{
        padding: 15px 15px;
    }
    .post__content{
        margin-top: 75px;
        color: black;
    }
    .post__content p{
        width: 44%;
        text-align: center;
        margin-left: 28%;
        line-height: 14px;
        color: #b2b2b2;
        letter-spacing: 1.5px;
        text-transform: uppercase;
        padding-top: 15px;
    }
    /* Drop Down Styles
================================ */
    .drop-down {
        list-style: none;
        overflow: hidden; /* When ul height is reduced, ensure overflowing li are not shown */
        color: black;
        margin-top: -15px;
        min-width: 250px;
        padding: 0;
        text-align: center;
        -webkit-transition: height 0.3s ease;
        transition: height 0.3s ease;
    }

    .drop-down.closed {
        /*  When toggled via jQuery this class will reduce the height of the ul which inconjuction
            with overflow: hidden set on the ul will hide all list items apart from the first */
        /* current li height 38px + 5px border */
        height: 43px;
    }

    .drop-down li {
        border-bottom: 1px solid #2c3e50;
    }

    .drop-down li a {
        display: block;
        text-decoration: none;
        padding: 10px; /* Larger touch target area */
    }

    .drop-down li:first-child a:after {
        content: "\25BC";
        float: right;
        margin-left: -30px; /* Excessive -margin to bring link text back to center */
        margin-right: 5px;
    }
    .gallery-title{
        display: flex;
        margin-left: 50%;
        transform: translateX(-50%);

    }


    .imageModal {
        display: none;
        position: fixed;
        z-index: 100;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.9);
        overflow: auto;
    }
</style>

